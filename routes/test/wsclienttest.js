const ws = require('ws');

const client = new ws('ws://localhost:3000');

client.on('connection', () => {
  // Causes the server to print "Hello"
  client.send('Hello');
});