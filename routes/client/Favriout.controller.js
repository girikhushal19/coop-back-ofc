const Favriout = require("../../models/client/Favriout.js");
var mongoose = require('mongoose');

module.exports.savefavriout = async(req, res) => {
    const isexists=await Favriout.findOne({
        student_id: req.body.student_id,
        teacher_id: req.body.teacher_id,
    })
   if(isexists){
    return res.send({
        data: null,
        status: false,
        message: "Favriout existe déjà",
        errmessage: "",
    });
   }else{
    const favriout = new Favriout({
        student_id: req.body.student_id,
        teacher_id: req.body.teacher_id,
    });
    favriout.save().then((favriout) => {
        return res.send({
            data: favriout,
            status: true,
            message: "Favriout sauvé avec succès",
            errmessage: "",
        });
    }).catch((err) => {
        return res.send({
            status: false,
            message: "",
            error: err,
            errmessage: "Le sauvetage de Favriout a échoué"
        });
    })
   }
}
module.exports.getallfavriouts = (req, res) => {
        Favriout.find({}, (err, favriouts) => {
            if (err) {
                res.json({
                    status: false,
                    message: "",
                    errmessage: err.message,
                    data: null
                });
            } else {
                res.json({
                    status: true,
                    data: favriouts,
                    message: "",
                    errmessage: ""
                });
            }
        });
    }
module.exports.getfavrioutbyclientID = async(req, res) => {
        
        const student_id=req.params.student_id ;
        console.log("student_id",student_id)
       await Favriout.aggregate([
            {$match: { student_id :student_id } },
            {$project: {teacher_id:{"$toObjectId":"$teacher_id"},student_id:{"$toObjectId":"$student_id"}}},
            {$lookup: {
                from: "lawunits",
                localField: "teacher_id",
                foreignField: "_id",
                as: "teacher"
            }},
            {$project:{student_id:"$student_id",teacher_id:"$teacher_id",lawyer:{$arrayElemAt:["$teacher",0]}}},
        
        ]).exec((err, favriouts) => {
            console.log(favriouts,err)
            if (err) {
                        res.json({
                            status: false,
                            message: "",
                            errmessage: err.message,
                            data: null
                        });
                    } else {
                        res.json({
                            status: true,
                            data: favriouts,
                            message: "",
                            errmessage: ""
                        });
                    }
        })
        // console.log(req.params.student_id)
        // Favriout.find({ student_id: req.params.student_id }, (err, favriouts) => {
        //     if (err) {
        //         res.json({
        //             status: false,
        //             message: "",
        //             errmessage: err.message,
        //             data: null
        //         });
        //     } else {
        //         res.json({
        //             status: true,
        //             data: favriouts,
        //             message: "",
        //             errmessage: ""
        //         });
        //     }
        // });
    }
module.exports.deletefavorite = (req, res) => {
        Favriout.findOneAndDelete({  teacher_id: req.params.teacher_id,student_id:req.params.student_id }, (err, favriout) => {
            if (err) {
                res.json({
                    status: false,
                    message: "",
                    errmessage: err.message,
                    data: null
                });
            } else {
                res.json({
                    status: true,
                    message: "Favriout a été supprimé avec succès",
                    errmessage: "",
                    data: favriout
                });
            }
        });
    }
   