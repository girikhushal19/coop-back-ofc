const User = require("../../models/auth/user");
const Servicesprovided = require('../../models/client/Servicesprovided');
const Userrelativesmodel=require("../../models/client/User_relatives");
const bcrypt = require("bcryptjs");
module.exports.updateprofile = async(req, res) => {
  try{
  //console.log("req.body ", req.body);
  //return false;
    let user_photo;
    const admin_user_profile_folder=process.env.admin_user_profile_folder;
   if(req.file){
    //console.log(req.file)
    user_photo=admin_user_profile_folder+"/"+req.file.filename;
   }
    let {
        first_name,
        last_name,
        email,
        phone,
        dob,
        user_id,
        address,
        password,
        newpassword,gender,latitude,longitude
    }=req.body;
    if(latitude)
    {
      latitude = parseFloat(latitude);
    }
    if(longitude)
    {
      longitude = parseFloat(longitude);
    }

    const user = await User.findOne({ _id:user_id });
    //  console.log(user);
    //  return false;
    if(!user)
    {
      return res.send({
        status:false,
        message:"",
        errmessage:"Identifiant d'utilisateur invalide",
        data:null
      })
    }else{
      let encryptedPassword
        if(password)
        {
          encryptedPasswordtest = await bcrypt.hash(password, 10);
          encryptedPassword= await bcrypt.hash(newpassword, 10);
          
          const ispasswordcorrect=await bcrypt.compare(password, user.password);
          if(!ispasswordcorrect){
          return res.send({
              status:false,
              message:"",
              errmessage:"old password is not correct",
              data:null
            })
          }
        }
        let location = {};
        if(latitude != "" && latitude != null && latitude != undefined && longitude != "" && longitude != null && longitude != undefined )
        {
          location = {
            type: 'Point',
            coordinates: [latitude,longitude]
          }
        }else{
          location = user.location;
        }
        const data={
          ...(first_name&&{first_name}),
          ...(last_name&&{last_name}),
          ...(gender&&{gender}),
          ...(email&&{email}),
          ...(phone&&{phone}),
          ...(dob&&{dob}),
          ...(user_photo&&{user_photo}),
          ...(address&&{address}),
          ...(newpassword&&{password:encryptedPassword}),
          ...(location&&{location:location}),
        }
      await User.findByIdAndUpdate(user_id,data).then((user) => {
          res.send({
            data:user,
            status:true,
            message:"Profil mis à jour avec succès",
            errmessage:""
          })
      }).catch((err) => {
        res.send({
            status:false,
            message:"",
            errmessage:err.message,
            data:null
        })
      })
    }
  }catch(e){
    res.send({
      status:false,
      message:"",
      errmessage:e.message,
      data:null
    })
  }
}
module.exports.deleteprofile = async(req, res) => {
    const user_id=req.params.user_id;
    const user=await User.findById(user_id);
    if(user){
       user.is_deleted=true;
    user.email=user.email+"_del";
    user.save().then((result)=>{
      return res.send({
        status:true,
        message:"deleted successfully",

      });
    })
  }else{
    return res.send({
        status:false,
        message:"user not found",

      });
  }

}
module.exports.getprofile = async(req, res) => 
{
  try{
    //await Servicesprovided.aggregate
    const user_id=req.params.user_id;
    let count_Job = await Servicesprovided.count({
      client_id:user_id,
      payment_status:true,
      service_status:false,
      is_service_rejected:false,
      service_acceptance_status_by_client:true,
      service_acceptance_status_by_provider:true,
    });
    await User.findById(user_id).then((user) => {
        res.send({
            status:true,
            message:"Obtenir le profil de l'enregistrement du succès complet",
            count_Job:count_Job,
            user:user
        })
    }).catch((err) => {
        res.send({
            status:false,
            message:err.message,
            error:err
        })
    })

  }catch(e){
    res.send({
      status:false,
      message:e.message,
      error:e
    })
  }
    
}
module.exports.add_relatives = async(req, res) => {
    const {
        user_id,
        first_name,
        address,
        title,
        dob

    }=req.body;
    const data={
        ...(user_id&&{user_id}),
        ...(first_name&&{first_name}),
        ...(address&&{address}),
        ...(title&&{title}),
        ...(dob&&{dob}),
        ...(new Date()&&{date_of_registration:new Date()})
    }
   Userrelativesmodel.find({
       first_name:data.first_name,
   },(err,result)=>{
         if(err){
              res.send({
                status:false,
                message:"",
                data:null,
                errmessage:"L'ajout de parents a échoué"
              })
         }else if(result.length>0){
              res.send({
                status:false,
                message:"La parenté existe déjà",
                errmessage:"",
                data:null
              })
         }else{
              Userrelativesmodel.create(data).then((user) => {
                res.send({
                      data:user,
                         status:true,
                         message:"Ajout réussi de parents",
                         errmessage:""
                })
              }).catch((err) => {
                  res.send({
                         status:false,
                         message:"",
                         errmessage:"L'ajout de parents a échoué",
                         data:null
                  })
              })
         }
   })
}
module.exports.getuserrelatives=async(req,res)=>{
    const user_id=req.params.user_id;
    await Userrelativesmodel.find({user_id:user_id}).then((user) => {
        res.send({
            user:user,
            status:true,
            message:"Relatives fetched successfully"
        })
    }).catch((err) => {
        res.send({
            status:false,
            message:"Relatives fetching failed",
            error:err
        })
    })
}
