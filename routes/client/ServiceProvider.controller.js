const Servicesprovided = require('../../models/client/Servicesprovided');
const SettingModel = require('../../models/admin/SettingModel');
const Services=require("../../models/lawyer/LawyerServices");
const OffersModel=require("../../models/client/Offers");
const lawyermodel=require('../../models/lawyer/Lawunit');

const ContactHours=require("../../models/lawyer/ContactHours");
const Catagory=require("../../models/admin/Catagory");
const notificationpermissionmodel = require("../../models/NotificationPermission");
const {sendpushnotificationtouser}=require('../../modules/Fcm');
const adminmodel = require('../../models/admin/Admin');
const { sendmail } = require("../../modules/sendmail");
const User = require("../../models/auth/user");
const CoupensModel=require("../../models/lawyer/Coupens");
const GroupchatsModel = require('../../models/chat/GroupchatsModel');
const path = require("path");
const { default: mongoose } = require('mongoose');
const { debug } = require('console');
const base_url=process.env.BASE_URL;
module.exports.saveservice = (req, res) => {
  try{
    const student_id=req.body.client_id;
    const student_name=req.body.client_name;
    const teacher_id=req.body.provider_id;
    const title=req.body.title;
    const teacher_name=req.body.provider_name;
    const course_id=req.body.service_id;
    const course_name=req.body.service_name;
    const location=req.body.location;
    const catagory_id=req.body.catagory_id;
    const location_cor=req.body.location_cor;
    const contact=req.body.contact;
    const duration=req.body.duration;
    const duration_2=req.body.duration_2;
    const desc=req.body.desc;
    const type=req.body.type;
    const catagory=req.body.catagory;
    const noofproviders=req.body.noofproviders;

    let latitude = req.body.latitude;
    let longitude = req.body.longitude;
    const address=req.body.address;
    console.log("req.body ----   >>>   ", req.body);
    if(!latitude || !longitude || !location)
    {
      return res.send({
        status: false,
        message: "L'adresse ne doit être que l'adresse de Google",
      });
    }
    // const student_id=req.body.student_id;
    // const client_name=req.body.client_name;
    // const teacher_id=req.body.teacher_id;
    // const lawyer_name=req.body.lawyer_name;
    const date_availed=new Date(req.body.date_availed).toISOString();
    const time_availed=req.body.time_availed;
   
    
    const mode_of_service=req.body.mode_of_service;
    const docs=[];
    if (req.files?.docs) {
     for(let i=0;i<req.files.docs.length;i++){
      let file=req.files.docs[i];
      docs.push(file.filename)
     }
     }
     //console.log("docs",docs)
    const newService = new Servicesprovided({});
    newService.client_id=student_id;
    newService.client_name=student_name;
    newService.catagory_id=catagory_id;
    newService.catagory=catagory;
    newService.service_id=course_id;
    newService.service_name=course_name;
    newService.address=address;
    
    newService.title=title;
    newService.date_availed=date_availed;
    newService.time_availed=time_availed;
    newService.docs=docs;
   
    newService.noofproviders=noofproviders;
    newService.location=location
    const location_cor_obj={type:"Point",coordinates:[latitude,longitude]}
    newService.location_cor=location_cor_obj
   
    newService.contact=contact
    newService.desc=desc
    newService.duration=duration
    newService.duration_2=duration_2
    
    // console.log("location_cor_obj",location_cor_obj,"newService0",newService)
    // url:base_url+"/api/payment/"+req.body.payment_method+"/"+service._id+"/"+req.body.amount
    newService.save(async(err, service) => {
        if (err) {
            res.json({
                status: false,
                errmessage: err.message,
                data: null,
                message: ""
            });
        } else {
         //await brodcastservicetoallviableproviders(service).then((allviableproviders)=>{
          // console.log("result",result)
          return res.json({
            status: true,
            message: 'Rendez-vous pris avec succès',
            errmessage:"",
            data: service,
            //allviableproviders:allviableproviders
            
          });
        //})
          
        }
    }
    );
  }catch(e)
  {
    return res.json({
      status: false,
      message: e.message
  });
  }
}
  module.exports.bookaprovider=async(req,res)=>
  {
    try{
      //console.log(req.body);
      const service_id=req.body.service_id;
      const provider_name=req.body.provider_name;
      const provider_id=req.body.provider_id;
      const payment_method=req.body.payment_method;
      const amount=req.body.amount;
      const finalamount=req.body.finalamount;
      const promocode=req.body.promocode;
      const admin = await adminmodel.findOne({});
      const offer = await OffersModel.findOne({service_id:service_id,provider_id:provider_id});
      //console.log("offer ", offer);
      if(offer)
      {
        offer.offer_acceptance_by_client=true;
        offer.save();
      }
      const service=await Servicesprovided.findById(service_id);
      
      service.provider_id=provider_id;
      service.provider_name=provider_name;
      service.amount=amount;
      service.finalamount=finalamount;
      service.promo=promocode;
      service.commission_earned = admin?.commissions ? admin.commissions : 0;
      service.save().then((result)=>{
        return res.send({
          status:true,
          message:"success",
          url:base_url+"/api/payment/"+payment_method+"/"+result._id+"/"+result.finalamount
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message,
          url:""
        });
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message,
        url:""
      });
    }
    
  }
module.exports.trypaymentagain=async(req,res)=>{
  const id=req.body.service_id;
  const payment_method=req.body.payment_method;
  const service=await Servicesprovided.findById(id);
  if(service){
   if(service.payment_status){
   return res.send({
    status:false,
    message:"payment is already done",
    data:service
   })
   }else{
    if(service.finalamount){
      let url=base_url+"/api/payment/"+payment_method+"/"+service._id+"/"+service.finalamount;
      return res.send({
       status:true,
       message:"fetched successfully",
       url:url
      })
    }else{
      return res.send({
        status:true,
        message:"service data in not complete yet",
        url:null
       })
    }
   }
  }else{
   return res.send({
    status:false,
    message:"service does not exists",
    data:null
   })

  }
}
module.exports.getfailedpayments=async(req,res)=>{
  const provider_id=req.body.provider_id;
  const allfailedtransaction=await Servicesprovided.find({
    provider_id:provider_id,
    payment_status:false,
    is_rejected:false,
    payment_failed:true
  });
  return res.send({
    status:true,
    message:"fetched success",
    data:allfailedtransaction
  })
}

module.exports.getAllSuccessPaymentUserId=async(req,res)=>
{
  try{
    const user_id=req.body.user_id;
    const allfailedtransaction=await Servicesprovided.find({
      client_id:user_id,
      payment_status:true,
      is_rejected:false,
      payment_failed:false
    });
    if(allfailedtransaction.length > 0)
    {
      return res.send({
        status:true,
        message:"Obtenir un succès complet des données",
        data:allfailedtransaction
      })
    }else{
      return res.send({
        status:false,
        message:"Pas de données",
        data:[]
      })
    }
    
  }catch(e){
    return res.send({
      status:false,
      message:e.message,
      data:[]
    })
  }
  
}
module.exports.getallservices=async(req,res)=>
  {
    try{
      var catagory=req.body.catagory;
      var longitude=req.body.longitude;
      var latitude=req.body.latitude;
      var radius=req.body.radius;
      if(!radius)
      {
        radius =  1000 * 100;
      }else{
        radius =  1000 * parseFloat(radius);
      }
      if(latitude != "" && longitude != "" && latitude != null && longitude != null)
      {
        latitude = parseFloat(latitude);
        longitude = parseFloat(longitude);
        var query =
        {
          "date_availed":{"$gte":new Date()},
          location_cor: {
            $near: {
            $maxDistance: radius,
            $geometry: {
              type: "Point",
              coordinates:[latitude,longitude]
            }
            }
          }
        }
      }else{
        var query =
        {
          "date_availed":{"$gte":new Date()},
        }
      }
    
      if(catagory){
          //query["service_name"]=catagory;
          query["catagory_id"]=catagory;
      }
      //console.log(query)
      await Servicesprovided.find(query)
      .then((services)=>{
        if(services.length > 0)
        { 
          return res.json({
            status:true,
            message:"Obtenir le succès complet des données de service",
            errmessage:"",
            dataCount:services.length,
            data:services
          })
        }else{
          return res.json({
            status:false,
            message:"Aucune donnée trouvée",
            errmessage:"",
            dataCount:services.length,
            data:[]
          })
        }
        
      }).catch((e)=>{
        return res.json({
          status:false,
          message:e.message,
          errmessage:"",
          dataCount:0,
          data:[]
        })
      });
    }catch(e){
      return res.json({
        status:false,
        message:e.message,
        errmessage:"",
        dataCount:0,
        data:[]
      })
    }
  }


module.exports.getAllServicesByLocation=async(req,res)=>
{
  try{
    var catagory=req.body.catagory;
    var longitude=req.body.longitude;
    var latitude=req.body.latitude;
    var radius=req.body.radius;
    if(!radius)
    {
      radius =  1000 * 100;
    }else{
      radius =  1000 * parseFloat(radius);
    }
    if(latitude != "" && longitude != "" && latitude != null && longitude != null)
    {
      latitude = parseFloat(latitude);
      longitude = parseFloat(longitude);
      var query =
      {
        "date_availed":{"$gte":new Date()},
        location_cor: {
          $near: {
          $maxDistance: radius,
          $geometry: {
            type: "Point",
            coordinates:[longitude,latitude]
          }
          }
        }
      }
    }else{
      var query =
      {
        "date_availed":{"$gte":new Date()},
      }
      return res.send({
        status:false,
        message:"L'adresse doit être celle de Google",
        errmessage:"",
        dataCount:0,
        data:[]
      })
    }
    query["payment_status"]=false;
    query["service_status"]=false;
    query["is_service_rejected"]=false;
    //query["provider_id"]='';
     
    
    //
    if(catagory){
        //query["service_name"]=catagory;
        query["catagory_id"]=catagory;
    }
    //console.log(query)
    //mongoose.set("debug",true);
    await Servicesprovided.find(query)
    .then((services)=>{
      if(services.length > 0)
      { 
        return res.json({
          status:true,
          message:"Obtenir le succès complet des données de service",
          errmessage:"",
          dataCount:services.length,
          data:services
        })
      }else{
        return res.json({
          status:false,
          message:"Aucune donnée trouvée",
          errmessage:"",
          dataCount:services.length,
          data:[]
        })
      }
      
    }).catch((e)=>{
      return res.json({
        status:false,
        message:e.message,
        errmessage:"",
        dataCount:0,
        data:[]
      })
    });
  }catch(e){
    return res.json({
      status:false,
      message:e.message,
      errmessage:"",
      dataCount:0,
      data:[]
    })
  }
}
module.exports.getservicesbyclientID=(req,res)=>{
    const student_id=req.params.student_id;
    Servicesprovided.find({student_id:student_id,payment_status:true},(err,services)=>{
        if(err){
            res.json({
                status:false,
                errmessage:err.message,
                data:null,
                message:""
            })
        }else{
            res.json({
                status:true,
                data:services,
                errmessage:"",
                message:"services fetched successfully"
            })
        }
    })
}
module.exports.getservicesbylawyerID=(req,res)=>{
  try{
    const teacher_id=req.params.teacher_id;
    Servicesprovided.find({teacher_id:teacher_id,payment_status:true},(err,services)=>{
        if(err){
          return res.send({
                status:false,
                message:"",
                errmessage:err.message,
                data:null
            })
        }else{
          return res.send({
                status:true,
                data:services,
                message:"",
                errmessage:""
            })
        }
    })
  }catch(e){
    return res.send({
      status:false,
      data:[],
      message:e.message,
      errmessage:""
  })
  }
}
module.exports.getservicesbyclientIDandlawyerID=(req,res)=>{
    const student_id=req.params.student_id;
    const teacher_id=req.params.teacher_id;
    Servicesprovided.find({student_id:student_id,teacher_id:teacher_id},(err,services)=>{
        if(err){
            res.json({
                status:false,
                message:"",
                errmessage:err.message,
                data:null
            })
        }else{
            res.json({
                status:true,
                data:services,
                message:"",
                errmessage:""
            })
        }
    })
}
module.exports.cancelservice=async(req,res)=>{
    const service_id=req.body.service_id;
    const student_id=req.body.client_id;
    const cancelation_reason=req.body.cancelation_reason;
    const cancelation_message=req.body.cancelation_message;
    let message=process.env.BOOKING_CANCELLED_BY_STUDENT
    const pathtofile = path.resolve("views/notifications/offercancelbyuser.ejs");
    Servicesprovided.findOneAndUpdate({_id:service_id,client_id:student_id},
      {service_acceptance_status_by_client:false,
      cancelation_reason_by_user:cancelation_reason,
      cancelation_message_by_user:cancelation_message},async(err,service)=>{
        if(err){
            return res.json({
                status:false,
                message:"err.message",
                errmessage:err.message,
                data:null
            })
        }else{
            const lawyerid=service.provider_id;
            const lawyer=await lawyermodel.findOne({_id:lawyerid})
            const MESSAGE_SERVICE_CANCELED_BY_CLIENT=process.env.MESSAGE_SERVICE_CANCELLED_BY_CLIENT;
            const notipermissions=await notificationpermissionmodel.find({id:service.provider_id});
            
            if(notipermissions.length){
                if(notipermissions[0].push_offer){
                    sendpushnotificationtouser(MESSAGE_SERVICE_CANCELED_BY_CLIENT+" "+service.client_name,lawyer,service.provider_id)
                }else{
                    console.log("push notification is disabled for student")
                }if(notipermissions[0].email_offer){
                    console.log(lawyer.email)
                    sendmail(pathtofile, 
                        { client_name: service.client_name,
                          reason:cancelation_reason,
                          message:cancelation_message,
                          adminmessage:message }, 
                        "Annulation du service par le client",
                        lawyer.email);
                }else{
                    console.log("email notification is disabled for student")
                }
            }else{
                console.log(lawyer.email)
                sendpushnotificationtouser(MESSAGE_SERVICE_CANCELED_BY_CLIENT+" "+service.client_name,lawyer,service.provider_id)
                sendmail(pathtofile, 
                  { client_name: service.client_name,
                    reason:cancelation_reason,
                    message:cancelation_message,
                    adminmessage:message }, 
                  "Annulation du service par le client",
                  lawyer.email);
            }
           
           
            // if(lawyer.fcm_token){
            //     sendpushnotificationtouser(MESSAGE_SERVICE_CANCELED_BY_CLIENT+" "+service.client_name,lawyer.fcm_token,service);
            // }
                return res.json({
                    status:true,
                    data:service,
                    message:"",
                    errmessage:""
                })
        }
    })
}
module.exports.getpricebylawyerid=async(req,res)=>{
    const type=req.params.type;
    const teacher_id=req.params.teacher_id;
    const lawyer=await lawyermodel.findOne({_id:teacher_id});
    let price;
    if(type=="online"){
      price=lawyer.modesofservice.online.price;
    }else if(type=="offline"){
     price=lawyer.modesofservice.offline.price;
    }
    res.send({
        status:true,
        message:"price fetched successfully",
        data:price,
        errmessage:""
    })
}

module.exports.getcompletedservicesbyclientID=async(req,res)=>
{ 
  try{
    const student_id=req.params.student_id;
    var d = new Date();
    d.setHours(0,0,0,0);
    console.log("client_id",student_id)
    const services=await Servicesprovided.aggregate([
        {$match:{
            client_id:student_id,
            payment_status:true,
            service_status:true,
            service_acceptance_status_by_client:true,
            service_acceptance_status_by_provider:true,
            is_service_rejected:false,
            //date_availed:{$lt:d}
          }},
        {$addFields:{teacher_id:{"$toObjectId":"$provider_id"},stringteacherId:{"toString":"$client_id"},serviceidstr:{"$toString":"$_id"}}},
        {$lookup:{from:"lawunits",localField:"teacher_id",foreignField:"_id",as:"provider"}},
        {
          $lookup:{
            from:"catagories",
            let:{"cat_id":{"$toObjectId":"$catagory_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$cat_id"]
                  }
                }
              }
            ],
            as:"catRecord"
          }
        },
        {$lookup:{from:"reviews",
        let:{service_id_str:{"$toString":"$_id"}},
        pipeline:[
          {
            $match:{ $expr: { $eq: ["$service_id", "$$service_id_str"]}},
          
          },
        {$addFields:{obj_client_id:{"$toObjectId":"$client_id"}}},
          {
            $lookup:{
              from:"users",
              localField:"obj_client_id",
              foreignField:"_id",
              as:"user"
            }
          }
    
        ],
        as:"ratings"
      }}
    ])
    return res.send({
        status:true,
        message:"Les états de service sont pleinement réussis",
        dataCount:services.length,
        data:services,
        errmessage:""
    })
  }catch(e){
    return res.send({
      status:false,
      message:e.message,
      data:[],
      errmessage:e.message
    })
  }
}

module.exports.getSingleService=async(req,res)=>
{ 
  try{
    const id=req.params.id;
    var d = new Date();
    d.setHours(0,0,0,0);
     
    const services=await Servicesprovided.aggregate([
        {$match:{
            _id: mongoose.Types.ObjectId(id)
          }},
        {$addFields:{teacher_id:{"$toObjectId":"$provider_id"},stringteacherId:{"toString":"$client_id"},serviceidstr:{"$toString":"$_id"}}},
        {$lookup:{from:"lawunits",localField:"teacher_id",foreignField:"_id",as:"provider"}},
        {
          $lookup:{
            from:"catagories",
            let:{"cat_id":{"$toObjectId":"$catagory_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$cat_id"]
                  }
                }
              }
            ],
            as:"catRecord"
          }
        },
        {$lookup:{from:"reviews",
        let:{service_id_str:{"$toString":"$_id"}},
        pipeline:[
          {
            $match:{ $expr: { $eq: ["$service_id", "$$service_id_str"]}},
          
          },
        {$addFields:{obj_client_id:{"$toObjectId":"$client_id"}}},
          {
            $lookup:{
              from:"users",
              localField:"obj_client_id",
              foreignField:"_id",
              as:"user"
            }
          }
    
        ],
        as:"ratings"
      }}
    ])
    return res.send({
        status:true,
        message:"Les états de service sont pleinement réussis",
        dataCount:services.length,
        data:services,
        errmessage:""
    })
  }catch(e){
    return res.send({
      status:false,
      message:e.message,
      data:[],
      errmessage:e.message
    })
  }
}


module.exports.getAmountByServiceId=async(req,res)=>{
  try{
    let {service_id,hours} = req.body;
    if(!service_id)
    {
      return res.send({
        status:false,
        message:"L'identifiant de la catégorie est requis"
      })
    }
    // if(!provider_id)
    // {
    //   return res.send({
    //     status:false,
    //     message:"L'identifiant du prestataire est requis"
    //   })
    // }
    if(!hours)
    {
      return res.send({
        status:false,
        message:"L'heure est requis"
      })
    }
    hours = parseFloat(hours);
    let stng_rec = await SettingModel.findOne({attribute_key: 'tax_percent'});
    let tax_percentage = 10;
    if(stng_rec)
    {
      tax_percentage = parseFloat(stng_rec.attribute_value);
    }
    let service_rec = await Services.findOne({_id:service_id},{price:1});
    if(!service_rec)
    {
      return res.send({
        status:false,
        message:"Identifiant de service invalide"
      })
    }else{
      //console.log(service_rec);
      let amount = hours * service_rec.price;
      let tax_value = amount * tax_percentage / 100;
      let final_amount = amount + tax_value;
      let arr = {amount:amount,tax_value:tax_value,final_amount:final_amount};
      return res.send({
        status:true,
        message:"Succès",
        data:arr
      })
    }
  }catch(e){
    return res.send({
      status:false,
      message:e.message
    })
  }
};
  module.exports.getongoingservicesbyclientID=async(req,res)=>
  {
    try{
      const student_id=req.params.student_id;
      var d = new Date();
              d.setHours(0,0,0,0);
              console.log("id",student_id,"d",d);
      const services=await Servicesprovided.aggregate([
          {$match:{
              client_id:student_id,
              //payment_status:true,
              service_status:false,
              is_service_rejected:false,
              // service_acceptance_status_by_client:true,
              // service_acceptance_status_by_provider:true,
              date_availed:{$gte:d}
            }},
            {
              $lookup:{
                from:"catagories",
                let:{"cat_id":{"$toObjectId":"$catagory_id"}},
                pipeline:[
                  {
                    $match:{
                      $expr:{
                        $eq:["$_id","$$cat_id"]
                      }
                    }
                  }
                ],
                as:"catRecord"
              }
            },
            {$addFields:{provider_id:{"$toObjectId":"$provider_id"},service_idSTR:{"$toString":"$_id"}}},
            {$lookup:{from:"lawunits",localField:"provider_id",foreignField:"_id",as:"provider"}},
            {$lookup:{
              from:"offers",
              let:{provider_id:{"$toObjectId":"$provider_id"},service_id:"$service_idSTR"},
              pipeline:[{
                $match: { $expr: { $eq: ["$service_id", "$$service_id"] } },
              
              },
          
              {$lookup:{
                from:"lawunits",
                let:{provider_id:{"$toObjectId":"$provider_id"}},
                pipeline:[{
                  $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
                
                },
                {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
                {
                  $lookup:{
                    from:"reviews",
                    localField:"providerIDSTR",
                    foreignField:"provider_id",
                    as:"reviews"
                  }
                },
                { $addFields: {
                                            
                  avgRating:{
                     $avg: "$reviews.rating"
                  }
              }},
            
              ],
                as:"provider"
              }},
            ],
              as:"offers"
            }},
            // {$addFields:{acceptedoffers:{
            //   $function:{
            //     body:`function(offers,service_idSTR){
            //       const acceptedoffers=[];
            //       for(let i=0;i<offers.length;i++){
            //         let offer=offers[i];
            //         if(offer.offer_acceptance_by_client && offer.service_id==service_idSTR){
            //           acceptedoffers.push(offer);
            //         }
            //       }
            //       return acceptedoffers;
            //     }`,
            //     args: [
            //       "$offers",
            //       "$service_idSTR"
            //     ],
            //     lang: "js",
            //   }

              
            // }}},
            // { "$group": {
            //   "_id": "$catagory",
            //   "name": { "$first": "$catagory" },  //$first accumulator 
            //   "count": { "$sum": 1 },  //$sum accumulator
            //   items: {$push: '$$ROOT'}
            // }},
            
          
        
      ]);
      if(services.length > 0)
      {
        return res.send({
          status:true,
          message:"Les états de service sont pleinement réussis",
          dataCount:services.length,
          data:services,
          errmessage:""
        })
      }else{
        return res.send({
          status:false,
          message:"Aucune donnée n'a été trouvée",
          dataCount:services.length,
          data:[],
          errmessage:""
        })
      }
    }catch(e){
      return res.send({
        status:false,
        message:e.message,
        dataCount:0,
        data:[],
        errmessage:e.message
      })
    }
    
  }


  module.exports.getCountOngoingCompleted=async(req,res)=>
  { 
    try{
      const student_id=req.params.student_id;
      var d = new Date();
      d.setHours(0,0,0,0);
      console.log("client_id",student_id)
      let completed = await Servicesprovided.count({
           
              client_id:student_id,
              payment_status:true,
              service_status:true,
              service_acceptance_status_by_client:true,
              service_acceptance_status_by_provider:true,
              is_service_rejected:false,
              date_availed:{$lt:d}
            } 
           );
      let onGoing = await Servicesprovided.count({
            client_id:student_id,
            //payment_status:true,
            service_status:false,
            is_service_rejected:false,
            // service_acceptance_status_by_client:true,
            // service_acceptance_status_by_provider:true,
            date_availed:{$gte:d}
          });
           
      let aa = onGoing + completed;
      return res.send({
          status:true,
          message:"Les états de service sont pleinement réussis", 
          totalCount:aa,
          errmessage:""
      })
    }catch(e){
      return res.send({
        status:false,
        message:e.message,
        totalCount:0,
        errmessage:e.message
      })
    }
  }

module.exports.getcompletedservicesbylawyerID=async(req,res)=>{
  try{
    const teacher_id=req.body.provider_id;
    const doc_name=req.body.doc_name;
    const  client_name=req.body.client_name;
    var start_date = req.body.start_date;
              var end_date = req.body.end_date;
              var d = new Date();
              d.setHours(0,0,0,0);
    let matchdata={
      provider_id:teacher_id,
      payment_status:true,
      //date_availed:{$lt:d},
      service_acceptance_status_by_client:true,
      service_acceptance_status_by_provider:true,
        is_service_rejected:false,
        service_status:true
      }
    let student_ids_having_this_name;
    if(client_name){
        matchdata["client_name"]={$regex:client_name,$options:"i"}
      // student_ids_having_this_name=await User.find(
      // {$or:[{first_name:{$regex:client_name,$options:'i'}},
      // {last_name:{$regex:client_name,$options:'i'}},
      // {$and:[{first_name:{$regex:client_name.split(" ")[0],$options:'i'}},{last_name:{$regex:client_name.split(" ")[1],$options:'i'}}]}]});
      // matchdata['student_id']={$in:student_ids_having_this_name.map(client=>client._id.toHexString())};
    }
    if(doc_name){
      matchdata['doc_name']={$regex:doc_name,$options:'i'};
    }
    if (start_date != '' || end_date != '') {
      if (start_date == '') {
          start_date = new Date(end_date);
          start_date = start_date - 1;
          end_date = new Date(end_date);
          end_date = end_date.setHours(11, 59, 59, 999);
          end_date = new Date(end_date);
          matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
      } else if (end_date == '') {
          end_date = new Date(start_date);
          end_date = end_date.setHours(11, 59, 59, 999);
          end_date = new Date(end_date);
          start_date = new Date(start_date);
          start_date = start_date - 1;
          start_date = new Date(start_date);
          console.log(start_date, end_date);
          matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
      } else {
          start_date = new Date(start_date);
          start_date = start_date.setHours(0, 0, 0, 0);
          start_date = new Date(start_date);
          end_date = new Date(end_date);
          end_date = end_date.setHours(11, 59, 59, 999);
          end_date = new Date(end_date);
          matchdata['date_availed'] ={$gte: start_date, $lt: end_date};
      }
    }
    //console.log(matchdata)
    const services=await Servicesprovided.aggregate([
        {$match:matchdata},
        {$addFields:{client_id:{"$toObjectId":"$client_id"}}},
        {$lookup:{from:"users",localField:"client_id",foreignField:"_id",as:"user"}}
    ])
    if(services.length > 0)
    {
      return res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
      })
    }else{
      return res.send({
        status:false,
        message:"Pas d'enregistrement",
        data:[],
        errmessage:""
      })
    }
    
  }catch(e){
    return res.send({
      status:false,
      message:e.message,
      data:[],
      errmessage:""
    })
  }
}
module.exports.getCommentedservicesbylawyerID=async(req,res)=>{
  try{
    let provider_id = req.body.provider_id;
    var d = new Date();
    d.setHours(0,0,0,0);
    if(!provider_id)
    {
      return res.send({
        status:false,
        message:"L'identifiant du prestataire est requis"
      })
    }
    let comment_rec = await GroupchatsModel.aggregate([
      {
        $match:{
          sender_id:provider_id
        }
      },
      {
        $lookup:{
          from:"servicesprovideds",
          let:{"ad_obj_id":{"$toObjectId":"$ad_id"}},
          pipeline:[
            {
              $match:{
                $expr:{
                  $and:[
                    {$eq:["$_id","$$ad_obj_id"]},
                    {$eq:["$service_acceptance_status_by_provider",false]},
                    {$eq:["$service_acceptance_status_by_client",false]},
                    {$eq:["$is_service_rejected",false]},
                    {$eq:["$service_status",false]},
                    {$eq:["$payment_status",false]},
                    {$gte:["$date_availed",d]}
                  ]
                }
              }
            }
          ],
          as:"service_job_record"
        }
      },
      {
        $match:{
            "service_job_record":{$ne:[] }
        }
      }
    ]);
    if(comment_rec.length > 0)
    {
      return res.send({
        status:true,
        message:"Succès",
        data:comment_rec
      })
    }else{
      return res.send({
        status:false,
        message:"Aucune donnée trouvée",
        data:comment_rec
      })
    }
  }catch(e){
    return res.send({
      status:false,
      message:e.message
    })
  }
}
module.exports.getongoingservicesbylawyerID=async(req,res)=>
{
  try{
    const teacher_id=req.body.provider_id;
    const doc_name=req.body.doc_name;
      const  client_name=req.body.student_name;
      var start_date = req.body.start_date;
      var end_date = req.body.end_date;
      var d = new Date();
      d.setHours(0,0,0,0);
      let matchdata={
        provider_id:teacher_id,
        payment_status:true,
        service_status:false,
        is_service_rejected:false,
        service_acceptance_status_by_client:true,
        service_acceptance_status_by_provider:true,
        date_availed:{$gte:d}
      }
      let student_ids_having_this_name;
      if(client_name)
      {
          matchdata["student_name"]={$regex:client_name,$options:"i"}
        // student_ids_having_this_name=await User.find(
        // {$or:[{first_name:{$regex:client_name,$options:'i'}},
        // {last_name:{$regex:client_name,$options:'i'}},
        // {$and:[{first_name:{$regex:client_name.split(" ")[0],$options:'i'}},{last_name:{$regex:client_name.split(" ")[1],$options:'i'}}]}]});
        // matchdata['student_id']={$in:student_ids_having_this_name.map(client=>client._id.toHexString())};
      }
      if(doc_name){
        matchdata['doc_name']={$regex:doc_name,$options:'i'};
      }
      if (start_date != '' || end_date != '') {
        if (start_date == '') {
            start_date = new Date(end_date);
            start_date = start_date - 1;
            end_date = new Date(end_date);
            end_date = end_date.setHours(11, 59, 59, 999);
            end_date = new Date(end_date);
            matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
        } else if (end_date == '') {
            end_date = new Date(start_date);
            end_date = end_date.setHours(11, 59, 59, 999);
            end_date = new Date(end_date);
            start_date = new Date(start_date);
            start_date = start_date - 1;
            start_date = new Date(start_date);
            console.log(start_date, end_date);
            matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
        } else {
            start_date = new Date(start_date);
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
            end_date = new Date(end_date);
            end_date = end_date.setHours(11, 59, 59, 999);
            end_date = new Date(end_date);
            matchdata['date_availed'] ={$gte: start_date, $lt: end_date};
        }
      }
      //console.log(matchdata)
      //mongoose.set("debug",true);
      const services=await Servicesprovided.aggregate([
        {$match:matchdata},
        {$addFields:{client_id:{"$toObjectId":"$client_id"}}},
        {$lookup:{from:"users",localField:"client_id",foreignField:"_id",as:"user"}},
      ])
      res.send({
          status:true,
          message:"Services recherchés avec succès",
          data:services,
          errmessage:""
      })
    }catch(e){
    return res.send({
      status:false,
      message: e.message,
      data:[],
      errmessage:""
  })
  }
}
module.exports.markCompleteJobFromProvider=async(req,res)=>{
  try{
    let {service_id,provider_id} = req.body;
    if(!service_id)
    {
      return res.send({
        status:false,
        message:"L'identifiant du service est requis"
      });
    }
    if(!provider_id)
    {
      return res.send({
        status:false,
        message:"L'identifiant du prestataire est requis"
      });
    }
    let check_service = await Servicesprovided.findOne({_id:service_id},{_id:1});
    //console.log("check_service ", check_service);
    if(!check_service)
    {
      return res.send({
        status:false,
        message:"L'identifiant du service n'est pas valide"
      });
    }
    let check_service_provider = await Servicesprovided.findOne({_id:service_id,provider_id:provider_id} );
    if(!check_service_provider)
    {
      return res.send({
        status:false,
        message:"L'identifiant du fournisseur n'est pas valide"
      });
    }

    if(check_service_provider.service_status == true)
    {
      return res.send({
          status:false,
          message:"Ce service est déjà prêt"
      });
    }else{
      // await Servicesprovided.updateOne({_id:service_id},{service_status: true}).then((result)=>{
      //   return res.send({
      //     status:true,
      //     message:"Le service est maintenant terminé "
      //   });
      // }).catch((e)=>{
      //   return res.send({
      //     status:false,
      //     message:e.message
      //   });
      // });
        await Servicesprovided.findByIdAndUpdate({_id:service_id},{service_status:true}).then(async(booking)=>{
          const lawyer = await lawyermodel.findById(provider_id);
          
          let totalpendingamount = lawyer.totalpendingamount+booking.payment_amount;

          await lawyermodel.findByIdAndUpdate({_id:provider_id},{totalpendingamount:totalpendingamount}).then((some)=>{
          //   console.log("hereee");
          // console.log(lawyer);
          // console.log("booking ",booking);
          // return false;

              return res.send({
                  data:booking,
                  status:true,
                  message:"Service marqué comme effectué avec succès",
                  errmessage:""
              });
          });
        }).catch((err)=>{
          return res.send({
              status:false,
              message:"La mise à jour de l'état de service n'a pas abouti",
              errmessage:err.message,
              data:null
          });
      });
    }
  }catch(e){
    return res.send({
      status:false,
      message:e.message
    });
  }
}
module.exports.getallbookeduserbylawyerID=async(req,res)=>{
    const teacher_id=req.params.teacher_id;
    const serviceprovider=await Servicesprovided.aggregate([
        {$match:{teacher_id:teacher_id, is_service_rejected:false}},
      
        {$project:{student_id:{"$toObjectId":"$student_id"},_id:0,teacher_id:1}},
        {$group:{_id:"$student_id",count:{$sum:1}}},
        {$project:{student_id:{"$toObjectId":"$_id"}}},
        {$lookup:{from:"users",localField:"student_id",foreignField:"_id",as:"user"}},
       
        

    ])
    
    res.send({
        status:true,
        message:"users fetched successfully",
        data:serviceprovider,
        errmessage:""
    })
}

module.exports.getallbookedlawyerbyclientID=async(req,res)=>{
    const student_id=req.params.student_id;
    const serviceprovider=await Servicesprovided.aggregate([
        {$match:{student_id:student_id, is_service_rejected:false}},
      

        {$project:{teacher_id:{"$toObjectId":"$teacher_id"}}},
        {$group:{_id:"$teacher_id",count:{$sum:1}}},
        {$project:{student_id:{"$toObjectId":"$_id"}}},
        {$lookup:{from:"lawunits",localField:"student_id",foreignField:"_id",as:"lawyer"}},
       
        // {$project:{teacher_id:{"$toObjectId":"$teacher_id"}}},
        // {$lookup:{from:"lawunits",localField:"teacher_id",foreignField:"_id",as:"lawyer"}},

    ])
    res.send({
        status:true,
        message:"users fetched successfully",
        data:serviceprovider,
        errmessage:""
    })
}
module.exports.checkpromocode=async(req,res)=>{
    const promo=req.params.promo;
    
    const cdate=new Date();
    cdate.setHours(0,0,0,0);
    const cdatestr=cdate.toISOString()
    console.log("cdate",cdate,"promo",promo,cdatestr)
   
    const ispromovalid=await CoupensModel.aggregate([
       { $match:{
            code:promo,
        status:true,
        // start_date:{$gte:cdate},
        // end_date:{$lte:cdate}

        }},
        {$addFields:{"islimitexahusted":{$subtract:["$limite","$used"]}}},
        {$match:{"islimitexahusted":{$gt:0}}}

    ])
    //console.log(ispromovalid)
    if(ispromovalid.length>0){
        return res.send({
            status:true,
            message:"Code appliqué avec succès",
            data:ispromovalid
        })
    }else{
        return res.send({
            status:false,
            errmessage:"Code Coupen non valide ",
            data:null
        })
    }
   
}
module.exports.getallcatagorieslevelbylevel=async(req,res)=>{
    const id=req.body.id;
    const query={}
    if(id){
        query['mastercatagory']=mongoose.Types.ObjectId(id);
    }
    //console.log("query",query);
    const allcatagory=await Catagory.aggregate([
        {$match:query},
        {
            $lookup:{
                from:"catagories",
                localField:"_id",
                foreignField:"mastercatagory",
                as:"subCategory"
            }
        },
        { 
            $addFields: {
                subCatCount: {$size: "$subCategory"}
            }  
        } 
    ])
    return res.send({
        status:true,
        message:"catagory fetched successfully",
        data:allcatagory
    })
}
module.exports.getSingleCategoryWithCheck=async(req,res)=>{
    const id=req.body.id;
    const query={}
    if(id){
        query['_id']=mongoose.Types.ObjectId(id);
    }
    //console.log("query",query);
    const allcatagory=await Catagory.aggregate([
        {$match:query},
        {
            $lookup:{
                from:"catagories",
                localField:"_id",
                foreignField:"mastercatagory",
                as:"subCategory"
            }
        },
        { 
            $addFields: {
                subCatCount: {$size: "$subCategory"}
            }  
        } 
    ])
    return res.send({
        status:true,
        message:"catagory fetched successfully",
        data:allcatagory
    })
}
module.exports.getallcatagoriesallatonce=async(req,res)=>{
    const allcatagory=await Catagory.aggregate([
        // {$mathc:is_parent:true},
        {$match:{mastercatagory:null}},
        {
          $graphLookup:{
            "from": "catagories",
            "startWith": "$_id",
            "connectFromField": "_id",
            "connectToField": "mastercatagory",
            "as": "subs",
            "maxDepth": 20,
            "depthField": "level",
            
          },
         
        },
        { "$unwind": {
          "path": "$subs",
          "preserveNullAndEmptyArrays": true
      } },
     
        {
          $sort: {
            "subs.level": -1
          }
        },
        {
          $group: {
            _id: "$_id",
            price: {
              $first: "$price"
            },
            parent_id: {
              $first: "$mastercatagory"
            },
            title: {
              $first: "$title"
            },
            image: {
              $first: "$image"
            },
            subs: {
              $push: "$subs"
            }
          }
        },
        {
          $addFields: {
            subs: {
              $reduce: {
                input: "$subs",
                initialValue: {
                  currentLevel: -1,
                  currentLevelChildren: [],
                  previousLevelChildren: []
                },
                in: {
                  $let: {
                    vars: {
                      prev: {
                        $cond: [
                          {
                            $eq: [
                              "$$value.currentLevel",
                              "$$this.level"
                            ]
                          },
                          "$$value.previousLevelChildren",
                          "$$value.currentLevelChildren"
                        ]
                      },
                      current: {
                        $cond: [
                          {
                            $eq: [
                              "$$value.currentLevel",
                              "$$this.level"
                            ]
                          },
                          "$$value.currentLevelChildren",
                          []
                        ]
                      }
                    },
                    in: {
                      currentLevel: "$$this.level",
                      previousLevelChildren: "$$prev",
                      currentLevelChildren: {
                        $concatArrays: [
                          "$$current",
                          [
                            {
                              $mergeObjects: [
                                "$$this",
                                {
                                  subs: {
                                    $filter: {
                                      input: "$$prev",
                                      as: "e",
                                      cond: {
                                        $and:[{$eq: [
                                          "$$e.mastercatagory",
                                          "$$this._id"
                                        ]}
                                      ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          ]
                        ]
                      }
                    }
                  }
                }
              }
            }
          }
        },
        {
          $addFields: {
            subs: "$subs.currentLevelChildren"
          }
        },
        {
          $match: {
            mastercatagory: null
          }
        }
      ])
    return res.send({
        status:true,
        message:"catagory fetched successfully",
        data:allcatagory
    })
}


module.exports.getbrodcastservicetoallviableproviders=async(req,res)=>{
  let radius=80000;
  let service={
  "_id":"63771ae60c2b92de174c2230",
  "service_status":false,
  "payment_status":false,
  "catagory_id":"6381c56f1d843a17b0de501a",
  "location_cor":{"type":"Point","coordinates":[-87.1032333,45.80508939999999]},
  "docs":[],
  "service_acceptance_status_by_provider":false,
  "service_acceptance_status_by_client":true,
  "is_service_rejected":false,
  "client_id":"6353a7df9aeff1dbc25ece32",
  "client_name":"\"est",
  "provider_id":"63579aba43f698e04556868f",
  "provider_name":"rohitabc",
  "service_id":"6329c48dfa0b79d8e8826198",
  "service_name":"relativityvdf",
  "date_availed":"2022-11-30T22:00:00.000Z",
  "time_availed":"10:00",
  "noofproviders":3,
  "location":"jaipur",
  "contact":"3625143652",
  "desc":"omething about job",
  "duration":"1H00"
  ,"__v":0}
  const dayofweek=new Date(service.date_availed).getDay();
  //matching of schedule
  const matchedcontacthours=await ContactHours.find({dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")});
  const matchedcontacthoursids=matchedcontacthours.map(hour=>hour.provider_id);
  
  //getting viable providers
  const allproviders=await Services.find({"catagory_id":service.catagory_id});
  const uniqueProvidersSet=new Set();
  for(let i=0;i<allproviders.length;i++){
    let providerid=allproviders[i].provider_id;
    uniqueProvidersSet.add(providerid);
  }
  const uniqueProvidersArr = [...uniqueProvidersSet];
  
  //merging both and selecting common among two arrays
  const finalproviderids=uniqueProvidersArr.filter(c=>matchedcontacthoursids.some(s=>s==c));
  const finalproviderOBJIDS=finalproviderids.map(e=>mongoose.Types.ObjectId(e))
  console.log("{dayNumber:dayofweek,time_availible:",{dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")})
  console.log("matchedcontacthoursids",matchedcontacthoursids);
  // console.log("uniqueProvidersSet",uniqueProvidersSet);
  console.log("uniqueProvidersArr",uniqueProvidersArr);
  console.log("finalproviderids",finalproviderids);
  console.log("finalproviderOBJIDS",finalproviderOBJIDS);
  const query={_id:{"$in":finalproviderids},
  location_cor: {
      $near: {
       $maxDistance: radius,
       $geometry: {
        type: "Point",
        coordinates:[service.location_cor.coordinates[0], 
        service.location_cor.coordinates[1]]
       }
      }
     }
  }
  const allviableproviders=await lawyermodel.find(query);
  const allviableprovidersids=allviableproviders.map(e=>e._id);
  const allviableproviderswithreviews=await lawyermodel.aggregate([
    {$match:{_id:{$in:allviableprovidersids}}},
    {$addFields:{providerSTRId:{"$toString":"$_id"}}},
    {$lookup:{
      from:"servicesprovideds",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
          $match:{
            $expr:{
              $eq:[
                "$provider_id","$$provider_id_str"
              ]
            },
            service_status:true
          }
        }
      ],
      as:"services"
    }},
    {$lookup:{
      from:"reviews",
      // localField:"providerSTRId",
      // foreignField:"provider_id",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
            $match: { $expr: { $eq: ["$provider_id", "$$provider_id_str"] }},
          
        },
        {
            $addFields: {
                
                obj_client_id: { "$toObjectId": "$client_id" }
            }
        },
        {
        $lookup: {
            from: "users",
            localField: "obj_client_id",
            foreignField: "_id",
            as: "user"
            
        }
    },
      ],
      as:"ratings"
    }},
    { $addFields: {
                                
        avgRating:{
          $round:[ {$avg: "$ratings.rating"},0]
        }
    }},
 
  ])
  // console.log("allviableprovidersids",allviableprovidersids,"allviableproviderswithreviews",allviableproviderswithreviews)
  const MESSAGE_JOB_CREATED=process.env.MESSAGE_JOB_CREATED;
  for(let j=0;j<allviableproviders.length;j++){
    let provider=allviableproviders[j];
    sendpushnotificationtouser(MESSAGE_JOB_CREATED+" "+service.client_name,provider,service.provider_id)
  }

return allviableproviderswithreviews;
}
module.exports.makeaoffer=async(req,res)=>
{
  try{
      const service_id=req.body.service_id;
      const provider_id=req.body.provider_id;
      
      let serv_rec = await Servicesprovided({_id:service_id},{catagory_id:1});

      const offer=await OffersModel.findOne({
        provider_id:provider_id,
        service_id:service_id
      });
      if(offer){
        return res.send({
          status:false,
          message:"L'offre existe déjà"
        })
      }
      else{

        let check_service = await Services.findOne({catagory_id:serv_rec.catagory_id,provider_id:provider_id},{_id:1});
        if(!check_service)
        {
          return res.send({
            status:false,
            message:"Vous ne disposez pas de ce service"
          })
        }else{
          const newDate=new Date();
          const newoffer=new OffersModel();
          newoffer.service_id=service_id;
          newoffer.provider_id=provider_id;
          newoffer.offer_date=newDate;
          newoffer.offer_time=newDate.getHours()+":"+newDate.getMinutes()+":"+newDate.getSeconds()+":"+newDate.getMilliseconds();
          newoffer.save().then((result)=>{
            return res.send({
              status:true,
              message:"L'offre a été acceptée",
              data:null
            })
          })
        }
      }
    }catch(e){
      return res.send({
        status:false,
        message:e.message,
        data:null
      })
    }
  }
module.exports.getAllOfferByProviderId = async(req,res)=>{
  try{
    let provider_id = req.body.provider_id;
    var d = new Date();
    d.setHours(0,0,0,0);
    const offers=await OffersModel.aggregate([
      {
        $match:{
          provider_id:provider_id
        }
      },
      {
        $lookup:{
          from:"servicesprovideds",
          let:{"ad_obj_id":{"$toObjectId":"$service_id"}},
          pipeline:[
            {
              $match:{
                $expr:{
                  $and:[
                    {$eq:["$_id","$$ad_obj_id"]},
                    {$eq:["$service_acceptance_status_by_provider",false]},
                    {$eq:["$service_acceptance_status_by_client",false]},
                    {$eq:["$is_service_rejected",false]},
                    {$eq:["$service_status",false]},
                    {$eq:["$payment_status",false]},
                    {$gte:["$date_availed",d]}
                  ]
                }
              }
            }
          ],
          as:"service_record"
        }
      },
      {
        $match:{
          "service_record":{$ne:[]}
        }
      }
    ]);
    if(offers.length > 0)
    {
      return res.send({
        status:true,
        data:offers,
        message:"fetched successfully"
      });
    }else{
      return res.send({
        status:false,
        data:[],
        message:e.message
      });
    }    
  }catch(e){
    return res.send({
      status:false,
      data:[],
      message:e.message
    });
  }
}
module.exports.getallofersbyserviceid=async(req,res)=>
{
  try{
    const service_id=req.body.service_id;
    const offers=await OffersModel.aggregate([
      {$match:{service_id:service_id}},
      {$lookup:{
        from:"lawunits",
        let:{provider_id:{"$toObjectId":"$provider_id"}},
        pipeline:[{
          $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
        
        },
        {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
        {
          $lookup:{
            from:"reviews",
            localField:"providerIDSTR",
            foreignField:"provider_id",
            as:"reviews"
          }
        },
        { $addFields: {
                                    
          avgRating:{
            $avg: "$reviews.rating"
          }
      }},
      {$addFields:{p_ID_STR:{"$toString":"$_id"}}},
      {
        $lookup:{
          from:"lawyerservices",
          let:{"pro_id":{"$toString":"$p_ID_STR"}},
          pipeline:[
            {
              $match: { $expr: { $eq: ["$provider_id", "$$pro_id"] } }
            }
          ],
          as:"provider_service"
        }
      },
      ],
        as:"provider"
      }},
      {$addFields:{
        serviceIDOBJ:{"$toObjectId":"$service_id"}
      }},
      {
        $lookup:{
        from:"servicesprovideds",
        localField:"serviceIDOBJ",
        foreignField:"_id",
        as:"service"
      }
    },
    {"$unwind": {
      "path": "$service",
      "preserveNullAndEmptyArrays": true
  }
  },
      {$addFields:{

        clientIDOBJ:{"$toObjectId":"$service.client_id"}
      }},
    {
      $lookup:{
      from:"users",
      localField:"clientIDOBJ",
      foreignField:"_id",
      as:"client"
    }
  },
    ]);
    if(offers.length > 0)
    {
      return res.send({
        status:true,
        data:offers,
        message:"fetched successfully"
      });
    }else{
      return res.send({
        status:false,
        data:[],
        message:e.message
      });
    }    
  }catch(e){
    return res.send({
      status:false,
      data:[],
      message:e.message
    });
  }
}

module.exports.getallservicesbyuserid=async(req,res)=>
{
  try{
    const client_id=req.body.client_id;
    const services=await Servicesprovided.aggregate([
      {$match:{client_id:client_id}},
      {$lookup:{
        from:"offers",
        let:{provider_id:{"$toObjectId":"$provider_id"}},
        pipeline:[{
          $match: { $expr: { $eq: ["$provider_id", "$provider_id"] } },
        
        },
        
        {$lookup:{
          from:"lawunits",
          let:{provider_id:{"$toObjectId":"$provider_id"}},
          pipeline:[{
            $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
          
          },
          {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
          {
            $lookup:{
              from:"reviews",
              localField:"providerIDSTR",
              foreignField:"provider_id",
              as:"reviews"
            }
          },
          { $addFields: {
                                      
            avgRating:{
              $round:[ {$avg: "$reviews.rating"},0]
            }
        }},
        ],
          as:"provider"
        }},
      ],
        as:"offers"
      }},
    ]);
    if(services.length > 0)
    {
      return res.send({
        status:false,
        dataCount:services.length,
        data:services,
        message:"Les états de service sont pleinement réussis"
      })
    }else{
      return res.send({
        status:false,
        message:"Aucune donnée n'a été trouvée",
        dataCount:services.length,
        data:[],
        errmessage:""
      })
    }
    
  }catch(e)
  {
    return res.send({
      status:false,
      message:e.message,
      dataCount:0,
      data:[],
      errmessage:e.message
    })
  }
}
module.exports.editservice=async(req,res)=>{
  
  const service_id=req.body.service_id;
    const title=req.body.title;
    const contact=req.body.contact;
   
    const desc=req.body.desc;
   
    // const student_id=req.body.student_id;
    // const client_name=req.body.client_name;
    // const teacher_id=req.body.teacher_id;
    // const lawyer_name=req.body.lawyer_name;
    console.log("req.body.date_availed",req.body)
    const date_availed=new Date(req.body.date_availed).toISOString();
    const time_availed=req.body.time_availed;
    const location=req.body.location;
    const location_cor=req.body.location_cor;
    
   
    const docs=[];
    if (req.files?.docs) {
     for(let i=0;i<req.files.docs.length;i++){
      let file=req.files.docs[i];
      docs.push(file.filename)
     }
     }
     console.log("docs",docs)
    const newService =await Servicesprovided.findById(service_id);
    if(!newService){
      return res.send({
        status:false,
        message:"no service found with this id"
      })
    }
    
    newService.title=title;
    newService.date_availed=date_availed;
    newService.time_availed=time_availed;
    newService.docs=docs;
   
   
    newService.location=location
    const location_cor_obj={type:"Point",coordinates:JSON.parse(location_cor)}
    newService.location_cor=location_cor_obj
   
    newService.contact=contact
    newService.desc=desc
   

    console.log("location_cor_obj",location_cor_obj,"newService0",newService)
    // url:base_url+"/api/payment/"+req.body.payment_method+"/"+service._id+"/"+req.body.amount
    newService.save(async(err, service) => {
        if (err) {
            res.json({
                status: false,
                errmessage: err.message,
                data: null,
                message: ""
            });
        } else {
          return res.json({
            status: true,
            message: 'Rendez-vous pris avec succès',
            errmessage:"",
            data: service
            
            
        });
          
        }
    }
    );
}

module.exports.getAllServiceByCategoryMatch=async(req,res)=>{
  try{
    let {category_id} = req.body;
    if(!category_id)
    {
      return res.send({
        status:false,
        message:"L'identifiant de la catégorie est requis"
      });
    }
    let record = await Services.aggregate([
      {
        $match:{
          catagory_id:category_id
        }
      },
      {
        $lookup:{
          from:"lawunits",
          let:{"provider_ObjId":{"$toObjectId":"$provider_id"}},
          pipeline:[
            {
              $match:{
                $expr:{
                  $eq:["$_id","$$provider_ObjId"]
                }
              }
            }
          ],
          as:"provider_rec"
        }
      },
      {
        $addFields:{
          "first_name":"$provider_rec.first_name",
          "last_name":"$provider_rec.last_name"
        }
      },
      {
        $project:{
          "provider_rec":0
        }
      }
    ]);
    if(record.length>0)
    {
      return res.send({
        status:true,
        message:"Succès",
        data:record
      });
    }else{
      //
      return res.send({
        status:false,
        message:"Aucun enregistrement n'a été trouvé",
        data:record
      });
    }
  }catch(e)
  {
    return res.send({
      status:false,
      message:e.message,
      data:[]
    });
  }


}
module.exports.getAllServiceByCategoryMatchSingle=async(req,res)=>{
  try{
    let {id} = req.params;
    if(!id)
    {
      return res.send({
        status:false,
        message:"L'identifiant est requis"
      });
    }
    let record = await Services.aggregate([
      {
        $match:{
          _id:mongoose.Types.ObjectId(id)
        }
      },
      {
        $lookup:{
          from:"lawunits",
          let:{"provider_ObjId":{"$toObjectId":"$provider_id"}},
          pipeline:[
            {
              $match:{
                $expr:{
                  $eq:["$_id","$$provider_ObjId"]
                }
              }
            }
          ],
          as:"provider_rec"
        }
      },
      {
        $addFields:{
          "first_name":"$provider_rec.first_name",
          "last_name":"$provider_rec.last_name"
        }
      },
      {
        $project:{
          "provider_rec":0
        }
      }
    ]);
    if(record.length>0)
    {
      return res.send({
        status:true,
        message:"Succès",
        data:record
      });
    }else{
      //
      return res.send({
        status:false,
        message:"Aucun enregistrement n'a été trouvé",
        data:record
      });
    }
  }catch(e)
  {
    return res.send({
      status:false,
      message:e.message,
      data:[]
    });
  }


}



module.exports.brodcastservicetoallviableproviders=async(req,res)=>{
  // console.log("service in broadcast",service);
  //matching of schedule
  try{
    //mongoose.set("debug",true);
    let {id} = req.params;
    let service = await Servicesprovided.findOne({_id:id});
    let radius=80000000;
    const dayofweek=new Date(service.date_availed).getDay();

    const matchedcontacthours = await ContactHours.find({dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")});

    const matchedcontacthoursids = matchedcontacthours.map(hour=>hour.provider_id);
    
    //getting viable providers
    const allproviders = await Services.find({"catagory_id":service.catagory_id});
    
    const uniqueProvidersSet=new Set();
    for(let i=0;i<allproviders.length;i++)
    {
      let providerid=allproviders[i].provider_id;
      uniqueProvidersSet.add(providerid);
    }
    const uniqueProvidersArr = [...uniqueProvidersSet];
    
    //merging both and selecting common among two arrays

    const finalproviderids = uniqueProvidersArr.filter(c=>matchedcontacthoursids.some(s=>s==c));
    const finalproviderOBJIDS = finalproviderids.map(e=>mongoose.Types.ObjectId(e))

    //console.log("{dayNumber:dayofweek,time_availible:",{dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")})

    //console.log("matchedcontacthoursids",matchedcontacthoursids);

    // console.log("uniqueProvidersSet",uniqueProvidersSet);
    //console.log("uniqueProvidersArr",uniqueProvidersArr);
    //console.log("finalproviderids",finalproviderids);
    //console.log("finalproviderOBJIDS",finalproviderOBJIDS);

    const query={_id:{"$in":finalproviderOBJIDS},
    location_cor: {
        $near: {
        $maxDistance: radius,
        $geometry: {
          type: "Point",
          coordinates:[service.location_cor.coordinates[0], 
          service.location_cor.coordinates[1]]
        }
        }
      }
    }
    const distancquery= {
      $geoNear: {
        near: { type: "Point",  coordinates:[service.location_cor.coordinates[0], 
        service.location_cor.coordinates[1]] },
        distanceField: "dist.calculated",
        maxDistance: radius,
      
        includeLocs: "dist.location_cor",
        spherical: true
      }
    }
    //console.log("query",distancquery)

    const allviableproviders = await lawyermodel.find(query);

    //console.log("allviableproviders",allviableproviders);
    //console.log("its all right")
    const allviableprovidersids=allviableproviders.map(e=>e._id);
    const allviableproviderswithreviews = await lawyermodel.aggregate([
      distancquery,
      {$match:{_id:{$in:allviableprovidersids}}},
    
      {$addFields:{providerSTRId:{"$toString":"$_id"}}},
    
      {
        $lookup:{
          //from:"servicesprovideds",
          from:"lawyerservices",
          let: { provider_id_str:{ "$toString": "$_id" } },
          
          pipeline:[
            {
              $match:{
                $expr:{
                  $eq:["$provider_id","$$provider_id_str"]
                },
              // service_status:true
              }
            }
          ],
          as:"services"
        }
      },
      {
        $addFields:{
          "serviceSize":{$size:"$services"}
        }
      },
      {$lookup:{
        from:"reviews",
        // localField:"providerSTRId",
        // foreignField:"provider_id",
        let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
        pipeline:[
          {
              $match: { $expr: { $eq: ["$provider_id", "$$provider_id_str"] }},
            
          },
          {
              $addFields: {
                  
                  obj_client_id: { "$toObjectId": "$client_id" }
              }
          },
          {
            $lookup: {
                from: "users",
                localField: "obj_client_id",
                foreignField: "_id",
                as: "user"
                
            }
          },
        ],
        as:"ratings"
      }},
      {
         $addFields:
         {
          avgRating:{
            $round:[ {$avg: "$ratings.rating"},0]
          }
        }
      }
    ])
    // console.log("allviableprovidersids",allviableprovidersids,"allviableproviderswithreviews",allviableproviderswithreviews)

    const MESSAGE_JOB_CREATED=process.env.MESSAGE_JOB_CREATED;

    for(let j=0;j<allviableproviders.length;j++)
    {
      let provider=allviableproviders[j];
      sendpushnotificationtouser(MESSAGE_JOB_CREATED+" "+service.client_name,provider,service.provider_id)
    }
    //return allviableproviderswithreviews;
    if(allviableproviderswithreviews.length > 0)
    {
      return res.send({
        status:true,
        message:"Succès",
        dataLength:allviableproviderswithreviews.length,
        data:allviableproviderswithreviews
      })
    }else{
      return res.send({
        status:false,
        message: "Pas de fournisseur disponible",
        data:[]
      })
    }
    
  }catch(e){
    //console.log(e.meesge);
    return res.send({
      status:false,
      message: e.message,
      data:[]
    })
  }
  
  
}
/*
async function brodcastservicetoallviableproviders(service){
  // console.log("service in broadcast",service);
  //matching of schedule
  try{
    let radius=80000000;
    const dayofweek=new Date(service.date_availed).getDay();

    const matchedcontacthours = await ContactHours.find({dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")});

    const matchedcontacthoursids = matchedcontacthours.map(hour=>hour.provider_id);
    
    //getting viable providers
    const allproviders = await Services.find({"catagory_id":service.catagory_id});
    
    const uniqueProvidersSet=new Set();
    for(let i=0;i<allproviders.length;i++)
    {
      let providerid=allproviders[i].provider_id;
      uniqueProvidersSet.add(providerid);
    }
    const uniqueProvidersArr = [...uniqueProvidersSet];
    
    //merging both and selecting common among two arrays

    const finalproviderids = uniqueProvidersArr.filter(c=>matchedcontacthoursids.some(s=>s==c));
    const finalproviderOBJIDS = finalproviderids.map(e=>mongoose.Types.ObjectId(e))

    console.log("{dayNumber:dayofweek,time_availible:",{dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")})

    console.log("matchedcontacthoursids",matchedcontacthoursids);

    // console.log("uniqueProvidersSet",uniqueProvidersSet);
    console.log("uniqueProvidersArr",uniqueProvidersArr);
    console.log("finalproviderids",finalproviderids);
    console.log("finalproviderOBJIDS",finalproviderOBJIDS);

    const query={_id:{"$in":finalproviderOBJIDS},
    location_cor: {
        $near: {
        $maxDistance: radius,
        $geometry: {
          type: "Point",
          coordinates:[service.location_cor.coordinates[0], 
          service.location_cor.coordinates[1]]
        }
        }
      }
    }
    const distancquery= {
      $geoNear: {
        near: { type: "Point",  coordinates:[service.location_cor.coordinates[0], 
        service.location_cor.coordinates[1]] },
        distanceField: "dist.calculated",
        maxDistance: radius,
      
        includeLocs: "dist.location_cor",
        spherical: true
      }
    }
    console.log("query",distancquery)

    const allviableproviders = await lawyermodel.find(query);

    console.log("allviableproviders",allviableproviders);
    console.log("its all right")
    const allviableprovidersids=allviableproviders.map(e=>e._id);
    const allviableproviderswithreviews = await lawyermodel.aggregate([
      distancquery,
      {$match:{_id:{$in:allviableprovidersids}}},
    
      {$addFields:{providerSTRId:{"$toString":"$_id"}}},
    
      {$lookup:{
        from:"servicesprovideds",
        let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
        pipeline:[
          {
            $match:{
              $expr:{
                $eq:[
                  "$provider_id","$$provider_id_str"
                ]
              },
              service_status:true
            }
          }
        ],
        as:"services"
      }},
      {$lookup:{
        from:"reviews",
        // localField:"providerSTRId",
        // foreignField:"provider_id",
        let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
        pipeline:[
          {
              $match: { $expr: { $eq: ["$provider_id", "$$provider_id_str"] }},
            
          },
          {
              $addFields: {
                  
                  obj_client_id: { "$toObjectId": "$client_id" }
              }
          },
          {
            $lookup: {
                from: "users",
                localField: "obj_client_id",
                foreignField: "_id",
                as: "user"
                
            }
          },
        ],
        as:"ratings"
      }},
      {
         $addFields:
         {
          avgRating:{
            $round:[ {$avg: "$ratings.rating"},0]
          }
        }
      }
    ])
    // console.log("allviableprovidersids",allviableprovidersids,"allviableproviderswithreviews",allviableproviderswithreviews)

    const MESSAGE_JOB_CREATED=process.env.MESSAGE_JOB_CREATED;

    for(let j=0;j<allviableproviders.length;j++)
    {
      let provider=allviableproviders[j];
      sendpushnotificationtouser(MESSAGE_JOB_CREATED+" "+service.client_name,provider,service.provider_id)
    }
    return allviableproviderswithreviews;
  }catch(e){
    console.log(e.meesge);
  }
  
  
}
*/