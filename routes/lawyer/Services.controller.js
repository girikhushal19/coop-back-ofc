const Services=require('../../models/lawyer/LawyerServices');
const Catagory=require("../../models/admin/Catagory");
module.exports.createservice = async(req, res) => {
  const {
    provider_id,
    provider_name,
    catagory_name,
    catagory_id,
    typeofjobs,
    title,
    experience,
    desc,
    price,
    diplomaname
    
  } = req.body;
 
  const catagory=await Catagory.findById(catagory_id);
  if(catagory){
    let course=new Services();
    course.provider_id=provider_id;
    course.title=title;
    course.provider_name=provider_name;
    course.catagory=catagory_name;
    course.catagory_id=catagory_id;
    course.desc=desc;
  
    course.price=price;
    course.experience=experience;
    course.typeofjobs=typeofjobs;
    course.image=catagory.image;
    course.diplomaname=catagory.diplomaname;
    
    course.save().then((service) => {
      return res.send({
      status:true,
      message:"service created successfully",
      data:service
    })}).catch((err) => { console.log(err); });
  }else{
    return res.send({
      status:true,
      message:"La catégorie n'existe pas",
      data:null
    })
  }
 
 
};
module.exports.getallservicebyLayerId = async(req, res) => {
  let lawyer_id=req.params.lawyer_id;

  await Services.find({provider_id:lawyer_id})
    .then((services) => {
      res.send({
        status:true,
        message:"courses fetched successfully",
        errmessage:"",
        dataLength:services.length,
        data:services
      });
    })
    .catch((err) => {
      res.send({
        status:true,
        message:"courses fetched successfully",
        errmessage:"something went wrond ,try again after sometime",
        data:services
      });
    });
};
module.exports.getServicebyId = async(req, res) => {
  const id = req.params.id;
 await Services.findById(id)
    .then((service) => {
     return res.send({
      status:true,
      data:service,
      message:"service fetched successfully"
     });
    })
    .catch((err) => {
      return res.send({
        status:true,
        data:null,
        message:err.message
       });
    });
};

module.exports.updateService = async(req, res) => {
 
  const {
    id,
   
    provider_name,
    catagory,
    subcatagory,
    title,
    diplomaname,
    experience,
    desc,
    price
    
    
  } = req.body;
  let image;
  let doc;
  
  if (req.files?.image) {
    if (req.files?.image) {
      // console.log("req.files.photo",req.files.photo)
      image = "userprofile/" + req.files.image[0].filename;
    } 
  }
  if (req.files?.doc) {
    if (req.files?.doc) {
      // console.log("req.files.photo",req.files.photo)
      doc = "userprofile/" + req.files.doc[0].filename;
    } 
  }
  if (req.files?.content) {
    if (req.files?.content) {
      // console.log("req.files.photo",req.files.photo)
      content = "userprofile/" + req.files.content[0].filename;
    } 
  }
  let data= {
   
            ...(title&&  {title:title} ),
            ...(catagory&&  {catagory:catagory} ),
            ...(subcatagory&&  {subcatagory:subcatagory} ),
            ...(desc&&  {desc:desc} ),
            ...(price&&  {price:price} ),
          
            ...(provider_name&&  {provider_name:provider_name} ),
           
            ...(diplomaname&&  {diplomaname:diplomaname} ),
            ...(experience&&  {experience:experience} ),
            ...(image&&  {image:image} ),
            ...(doc&&  {doc:doc} )
          
            
          
            
        }
      console.log("data",data,"req.body",req.body);
      await  Services.findByIdAndUpdate(
          id,
        data)
        .then((lawyer) => {
            res.send({"message":"updated successfully"});
        })
};
module.exports.deletecourse=async(req,res)=>{
  let id=req.params.id;
    let course=await Services.findByIdAndDelete(id);
    return res.send({
      status:true,
      message:"course deleted successfully",
      data:null,
      errmessage:""
    })
    
}

module.exports.updatescheduleforcourse=async(req,res)=>{
  // const date=req.body.date;
  // const id=req.body.id;
  // const time=req.body.time;
  // const dat_and_time_array=[date,time];
  // const couresemodel=Services.findById(id);
}
