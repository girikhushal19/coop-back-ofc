const Schedule = require("../../models/lawyer/Schedule");
const ScheduleC = require("../../models/lawyer/CourseSchedule");
const ContactHours = require("../../models/lawyer/ContactHours");
const mongoose=require("mongoose");
module.exports.setSchedule = async(req, res) => {
  console.log(req.body);
  const provider_id = req.body.id;
  const date = req.body.date;
  const modesofservice = req.body.modesofservice;
  const time = req.body.time;
  const dateinformat= new Date(date);
  await Schedule.findOne({ provider_id: provider_id, date: dateinformat }).then(( schedule) => {
  if (schedule) {
      res.status(200).send({
        status: false,
        message: "Le programme existe déjà",
        data: null,
        errmessage: null,
      });
    } else {
      const schedule = new Schedule();
      schedule.provider_id = provider_id;

      schedule.date = dateinformat;
      schedule.time = time;

      // modesofservice == "online"
      //   ? (schedule.modesofservice.online.time_availible = time)
      //   : (schedule.modesofservice.offline.time_availible = time);
      schedule.save((err, schedule) => {
        if (err) {
          res.status(200).send({
            message: "",
            data: null,
            errmessage: err.message,
            status: false,
          });
        } else {
          res.status(200).send({
            message: "Programme enregistré avec succès",
            status: true,
          });
        }
      });
    }
  });
};
module.exports.getSchedulebyID = async (req, res) => {
  const id = req.params.provider_id;
  // await Schedule.find({
  //   provider_id: id,
  // })
  var d = new Date();
    d.setHours(0,0,0,0);
  await Schedule.aggregate([{$match:{provider_id: id,date:{$gte:d}}},
  {$sort:{date:1}},

  ])
    .then((schedule) => {
      console.log(schedule);
      res.status(200).send({
        message: "schedule",
        status: true,
        data: schedule,
        errmessage:""
      });
    })
    .catch((err) => {
      if (err) {
        res.status(200).send({
          message:"",
          status: false,
          errmessage: err.message,
          data: null,
        });
      }
    });
};
module.exports.updateSchedule = async(req, res) => {
  const time = req.body.time;
  // const modesofservice = req.body.modesofservice;
  const schedule_id = req.body.schedule_id;
  const data = {
   ...(time&&{time:time})
  };
 await Schedule.findByIdAndUpdate(schedule_id, data, (err, schedule) => {
    if (err) {
      res.status(200).send({
        message:"",
        status: false,
        errmessage: err.message,
        data: null,
      });
    } else {
      res.status(200).send({
        message: "Programme mis à jour avec succès",
        status: true,
        errmessage: null,
        data: null,
      });
    }
  });
};
module.exports.deleteSchedule = (req, res) => {
  Schedule.findByIdAndRemove(req.params.schedule_id, (err, schedule) => {
    if (err) {
      res.status(200).send({
        message: "",
        status: false,
        errmessage: err.message,
        data: null,
      });
    } else {
      res.status(200).send({
        status:true,
        message:"Programme supprimé avec succès",
        errmessage:"",
        data:null
      });
    }
  });
};
module.exports.setContactHoursprev = (req, res) => {
  const provider_id = req.body.provider_id;
  const day = req.body.day;
  const time_availible = req.body.time_availible;
  ContactHours.findOne(
    { provider_id: provider_id, day: day },
    (err, conhours) => {
      if (err) {
        return res.status(200).send(err);
      } else if (conhours) {
          console.log(conhours);
          ContactHours.findByIdAndUpdate(conhours._id,{
            time_availible: time_availible

        }).then(()=>{
            return  res.status(200).send({
                message: "contact hours updated",
            });
        })
      } else{
            const contactHours = new ContactHours();
            contactHours.provider_id = provider_id;
            contactHours.day = day;
            contactHours.time_availible = time_availible;
            contactHours.save((err, contactHours) => {
                if (err) {
                    return res.status(200).send({
                    message: err.message,
                    status: false,
                });
                } else {
               return res.status(200).send({
                    message: "contact hours set",
                    status: true,
                });
                }
            });
      }
      
    }
  );
};

module.exports.setContactHours = async(req, res) => {
  try{
    const data = req.body.data;
    //console.log("data ", data);
    //console.log("data ", data.length);
    //return false;
    let last_val = data.length;
    //return false;
    let xx = 1;
  data?.map(async(item) => {

    const opening_hour_num=parseInt(item.opening_hour.split(":")[0])?parseInt(item.opening_hour.split(":")[0]):0;
            const closing_hour_num=parseInt(item.closing_hour.split(":")[0])?parseInt(item.closing_hour.split(":")[0]):0;

   await ContactHours.findOne(
      { provider_id: item.provider_id, day: item.day,opening_hour_num:opening_hour_num,closing_hour_num:closing_hour_num }).then( (conhours) => {
      if (conhours) {
            //console.log(conhours);
            const opening_hour_num=parseInt(item.opening_hour.split(":")[0])?parseInt(item.opening_hour.split(":")[0]):0;
            const closing_hour_num=parseInt(item.closing_hour.split(":")[0])?parseInt(item.closing_hour.split(":")[0]):0;
            ContactHours.findByIdAndUpdate(conhours._id,{
              opening_hour: item.opening_hour,
              closing_hour: item.closing_hour,
              opening_hour_num:opening_hour_num,
              closing_hour_num:closing_hour_num
  
          }).then(()=>{
              // return  res.status(200).send({
              //     message: "contact hours updated",
              // });
          })
        } else{
          const opening_hour_num=parseInt(item.opening_hour.split(":")[0])?parseInt(item.opening_hour.split(":")[0]):0;
            const closing_hour_num=parseInt(item.closing_hour.split(":")[0])?parseInt(item.closing_hour.split(":")[0]):0;
              const contactHours = new ContactHours();
              contactHours.provider_id = item.provider_id;
              contactHours.day = item.day;
              contactHours.dayNumber = item.dayNumber;
              // contactHours.time_availible = item.time_availible;
              contactHours.opening_hour = item.opening_hour;
              contactHours.closing_hour = item.closing_hour;
              contactHours.opening_hour_num=opening_hour_num,
              contactHours.closing_hour_num=closing_hour_num
              contactHours.save((err, contactHours) => {
                  if (err) {
                    console.log("err 211 line ", err.message);
                      // return res.status(200).send({
                      // message: err.message,
                      // status: false,
                    //});
                  } else {
                    // console.log("xx ", xx);
                    // if(last_val == xx)
                    // { 
                    //   // return res.status(200).send({
                    //   //   message: "contact hours set",
                    //   //   status: true,
                    //   // });
                    // }
                 
                  }
              });
        }
        
      });
      xx++;
  })
    return res.status(200).send({
      message: "Les heures de contact du prestataire sont gérées avec succès.",
      status: true,
    });
 
  }catch(err){
    res.status(200).send({
      message:"",
      errmessage: err.message,
      status: false,
      data: null,
    });
  }
};

module.exports.updateSpecificDay = async(req,res)=>{
  try{

    let {provider_id,day} = req.body;
    if(!provider_id)
    {
      return res.send({
        status:false,
        message:"L'identifiant du prestataire est requis"
      })
    }
    // opening_hour: '10:00',
    // closing_hour: '06:00',
    // opening_hour_num: 10,
    // closing_hour_num: 6,

    await ContactHours.updateMany({provider_id:provider_id,dayNumber:day},{opening_hour:'',closing_hour:'',opening_hour_num:0,closing_hour_num:0}).then((result)=>{
      return res.send({
        status:true,
        message:"Enregistrement complet des succès actualisés"
      })
    }).catch((e)=>{
      return res.send({
        status:false,
        message:e.message
      })
    })
  }catch(e){
    return res.send({
      status:false,
      message:e.message
    })
  }
}


module.exports.dateteSpecificTimeSloat = async(req,res)=>{
  try{

    let {id} = req.body;
    if(!id)
    {
      return res.send({
        status:false,
        message:"L'identifiant est requis"
      })
    }
    // opening_hour: '10:00',
    // closing_hour: '06:00',
    // opening_hour_num: 10,
    // closing_hour_num: 6,

    await ContactHours.deleteOne({_id:id}).then((result)=>{
      return res.send({
        status:true,
        message:"Enregistrer les succès supprimés dans leur intégralité"
      })
    }).catch((e)=>{
      return res.send({
        status:false,
        message:e.message
      })
    })
  }catch(e){
    return res.send({
      status:false,
      message:e.message
    })
  }
}

module.exports.getContactHoursbyID = async (req, res) => {
  try{
    const id = req.params.provider_id;
    const provider_id = req.params.provider_id;
    let check_data = await ContactHours.findOne({ provider_id: id  },{_id:1});
    if(check_data)
    {
      await ContactHours.find({
        provider_id: id,
      }).sort({dayNumber:1}).then((contactHours) => {
        //console.log(contactHours);
        res.status(200).send({
          message: "contact hours",
          status: true,
          data: contactHours,
        });
      }).catch((err) => {
        return res.status(200).send({
          message: err.message,
          status: false,
          data: [],
        });
      });
    }else{
      console.log("hereeeeeeeeee 342");
      let day_array = [
        {
          provider_id: provider_id,
          day: 'sunday',
          dayNumber: 0,
          opening_hour: '',
          closing_hour: '',
          opening_hour_num: 0,
          closing_hour_num: 0
        },
        {
          provider_id: provider_id,
          day: 'monday',
          dayNumber: 1,
          opening_hour: '',
          closing_hour: '',
          opening_hour_num: 0,
          closing_hour_num: 0
        },
        {
          provider_id: provider_id,
          day: 'tuesday',
          dayNumber: 2,
          opening_hour: '',
          closing_hour: '',
          opening_hour_num: 0,
          closing_hour_num: 0
        },
        {
          provider_id: provider_id,
          day: 'wednusday',
          dayNumber: 3,
          opening_hour: '10:00',
          closing_hour: '06:00',
          opening_hour_num: 0,
          closing_hour_num: 0
        },
        {
          provider_id: '63579abb43f698e04556868f',
          day: 'thursday',
          dayNumber: 4,
          opening_hour: '10:00',
          closing_hour: '06:00',
          opening_hour_num: 0,
          closing_hour_num: 0
        },
        {
          provider_id: provider_id,
          day: 'friday',
          dayNumber: 5,
          opening_hour: '',
          closing_hour: '',
          opening_hour_num: 0,
          closing_hour_num: 0
        },
        {
          provider_id: provider_id,
          day: 'saterday',
          dayNumber: 6,
          opening_hour: '',
          closing_hour: '',
          opening_hour_num: 0,
          closing_hour_num: 0
        }
      ];
      console.log("day_array.length ", day_array.length);
      // for(let x=0; x < day_array.length; x++)
      // {
      //   console.log(day_array[x].dayNumber);
      //   console.log(day_array[x].day);
      //   let add_data = {
      //     provider_id: provider_id,
      //     day: day_array[x].day,
      //     dayNumber: day_array[x].dayNumber,
      //     opening_hour: '',
      //     closing_hour: '',
      //     opening_hour_num: 0,
      //     closing_hour_num: 0
      //   };
        
      // }
      await ContactHours.insertMany(day_array);
      await ContactHours.find({
        provider_id: id,
      }).sort({dayNumber:1}).then((contactHours) => {
        //console.log(contactHours);
        res.status(200).send({
          message: "contact hours",
          status: true,
          data: contactHours,
        });
      }).catch((err) => {
        return res.status(200).send({
          message: err.message,
          status: false,
          data: [],
        });
      });
    }
  }catch(e){
    return res.status(200).send({
      message: e.message,
      status: false,
      data: [],
    });
  }
  
};
module.exports.updateContactHours = (req, res) => {
  const time_availible = req.body.time_availible;
  const contactHours_id = req.body.contact_hour_id;
  ContactHours.findByIdAndUpdate(
    contactHours_id,
    { time_availible: time_availible },
    (err, contactHours) => {
      if (err) {
        res.status(200).send({
          message: "",
          status: false,
          errmessage: err.message,
          data: null,
        });
      } else {
        res.status(200).send({
          message: "Heures de contact mises à jour",
          status: true,
          errmessage: null,
          data: null,
        });
      }
    }
  );
};
module.exports.deleteContactHours = (req, res) => {
  ContactHours.find({day: req.params.day}).remove((err, contactHours) => {
    if (err) {
      res.status(200).send({
        message: "",
        status: false,
        errmessage: err.message,
        data: null,
      });
    } else {
      res.status(200).send({
        message: "Heures de contact supprimées",
        status: true,
        errmessage: null,
        data: null,
      });
    }
  })
};


module.exports.setScheduleC = (req, res) => {
  console.log(req.body);
  const provider_id = req.body.provider_id;
  const course_id = req.body.course_id;
  const date = req.body.date;
  const modesofservice = req.body.modesofservice;
  const time = req.body.time;
  const dateinformat= new Date(date);
  ScheduleC.findOne({ course_id: course_id, date: dateinformat }, (err, schedule) => {
    if (err) {
      res.status(200).send({
        message: "",
        status: false,
        errmessage: err.message,
        data: null,
      });
    } else if (schedule) {
      res.status(200).send({
        status: false,
        message: "Le programme existe déjà",
        data: null,
        errmessage: null,
      });
    } else {
      const schedule = new ScheduleC();
      schedule.course_id = course_id;
      schedule.provider_id = provider_id;
      schedule.date = dateinformat;
      schedule.time = time;
    console.log("schedule",schedule)
      // modesofservice == "online"
      //   ? (schedule.modesofservice.online.time_availible = time)
      //   : (schedule.modesofservice.offline.time_availible = time);
      schedule.save((err, schedule) => {
        if (err) {
          res.status(200).send({
            message: "",
            data: null,
            errmessage: err.message,
            status: false,
          });
        } else {
          res.status(200).send({
            message: "Programme enregistré avec succès",
            status: true,
          });
        }
      });
    }
  });
};
module.exports.getSchedulebyIDC = async (req, res) => {
  const id = req.params.provider_id;
  // await Schedule.find({
  //   provider_id: id,
  // })
  var d = new Date();
    d.setHours(0,0,0,0);
  await ScheduleC.aggregate([{$match:{course_id: id,date:{$gte:d}}},
  {$sort:{date:1}},

  ])
    .then((schedule) => {
      console.log(schedule);
      res.status(200).send({
        message: "schedule",
        status: true,
        data: schedule,
        errmessage:""
      });
    })
    .catch((err) => {
      if (err) {
        res.status(200).send({
          message:"",
          status: false,
          errmessage: err.message,
          data: null,
        });
      }
    });
};
module.exports.updateScheduleC =async (req, res) => {
  const time = req.body.time;
  // const modesofservice = req.body.modesofservice;
  const schedule_id = mongoose.Types.ObjectId(req.body.schedule_id);
  const data = {
   ...(time&&{time:time})
  };
  console.log("data",data,time,time,req.body)
 const isok=await ScheduleC.findByIdAndUpdate(schedule_id, data);
 console.log("isok",isok)
 if (!isok) {
  return  res.status(200).send({
      message:"",
      status: false,
      errmessage:"error",
      data: null,
    });
  } else {
    return res.status(200).send({
      message: "Programme mis à jour avec succès",
      status: true,
      errmessage: null,
      data: null,
    });
  }
};
module.exports.deleteScheduleC = async(req, res) => {
 const isok=await ScheduleC.findByIdAndRemove(req.params.schedule_id);
 if (!isok) {
  res.status(200).send({
    message: "",
    status: false,
    errmessage:" error",
    data: null,
  });
} else {
  res.status(200).send({
    status:true,
    message:"Programme supprimé avec succès",
    errmessage:"",
    data:null
  });
}
};

