const multer = require("multer");
const path = require("path");
const jwt = require("jsonwebtoken");
const User = require("../../models/auth/user");
const Documentsforservice = require("../../models/client/Documentsforservice");
const client_documents_path=process.env.client_documents_path;
module.exports.senddocument = (req, res) => {
  let filename;
  const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path.resolve(client_documents_path));
    },
    filename: (req, file, cb) => {
     
      filename = Date.now() + "-" + file.originalname;
      cb(null, filename);
    },
  });
  let uploadStorage = multer({ storage: storage });
  let isuploaded = uploadStorage.single("file");
  isuploaded(req, res, (err) => {
    if (err) {
      res.send(err);
    } else {
      let { token } = req.body;

      jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
       if(err){
         res.send(err)
       }else{
          let userid = decoded.user_id;
        let lawyerid = req.body.lawyer_id;
        let doc_name = req.body.doc_name;
        let doc_path = "clientdocuments/"+ filename;
        Documentsforservice.create({
          client_id: userid,
          laywer_id: lawyerid,
          doc_name: doc_name,
          doc_path: doc_path,
        }).then((doc) => {
          res.send(doc);
        });
       }
      });
     
    }
  });
};

module.exports.getalldocument = (req, res) => {
  let token  = req.params.token;
  
  
  jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
    if(err){
      res.send(err)
    }
   else{
    let userid = decoded.user_id;
   
    Documentsforservice.find({ client_id: userid }).then((doc) => {
      res.send(doc);
    });
   }
  });
};
module.exports.getdocumentsbylawyerId = (req, res) => {
  let lawyer_id=req.params.lawyer_id;
  Documentsforservice.find({lawyer_id:lawyer_id}).then((doc)=>{
    res.send(doc);
  })
}

