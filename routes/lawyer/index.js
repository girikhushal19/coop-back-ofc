const express = require("express");
const { getallamount } = require("./Revenu.controller");
const multer = require("multer");
const path=require("path")
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const admin_images_path=process.env.admin_images_path;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_lawyer_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Math.floor(1000000000 + Math.random() * 9000000000) + path.extname(file.originalname)) //Appending extension
  }
})
const upload2 = multer({ storage: storage });

const LauthController= require("./Lauth.controller");
const SubscriptionsController=require("./Subscriptions.controller");
let Servicescontroller = require("./Services.controller");
const Ratingcontroller = require("./Rating.controller");
const Misccontroller=require("./Misc.controller");
const LawyerBankDetailsController=require("./LawyerBankDetails.controller");
const Bookingscontroller=require("./Bookings.controller");
let Schedulecontroller = require("./Schedule.controller");
const Lawyercontroller = require("./Lawyer.controller");
const RevenuController=require("./Revenu.controller");





const lawyerroutes = express.Router();


//Revenu
lawyerroutes.get("/getallamountbylawyerId/:lawyer_id", RevenuController.getallamount);
//Revenu

//auth
lawyerroutes.post("/register", LauthController.register);
lawyerroutes.post("/login", LauthController.login);
lawyerroutes.get("/logout/:id", LauthController.logout);
lawyerroutes.post("/forgot_password", LauthController.forgot_password);
lawyerroutes
  .route("/reset_password")
  .get(LauthController.render_reset_password_template)
  .post(LauthController.reset_password);
lawyerroutes.post("/verifycode", LauthController.verifycode);
lawyerroutes.post("/changepassword", LauthController.changepassword);
//auth

//subs
lawyerroutes.get("/getSubscriptionbylawyerIDred/:lawyer_id",SubscriptionsController.getSubscriptionbylawyerIDred)
lawyerroutes.post("/createSubscription", SubscriptionsController.createSubscription);
lawyerroutes.get("/getSubscription/:subscription_id", SubscriptionsController.getSubscriptionById);
lawyerroutes.get("/getSubscriptionbylawyerID/:lawyer_id", SubscriptionsController.getSubscriptionbylawyerID);
lawyerroutes.post("/updateSubscription", SubscriptionsController.updateSubscription);
lawyerroutes.get("/deleteSubscription/:subscription_id", SubscriptionsController.deleteSubscription);
lawyerroutes.get("/getallsubscriptions", SubscriptionsController.getAllSubscriptions);
//subs

//provider
lawyerroutes.get("/getsubscriptioninvoice/:sub_id", Lawyercontroller.getsubscriptioninvoice);
lawyerroutes.get("/getallsubscriptionpackages", Lawyercontroller.getallsubscriptionpackages);
lawyerroutes.post("/createprovider",upload2.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }, 
  {
    name:'drivinglicencefront',
    maxCount: 1
  },
  {
    name:'drivinglicenceback',
    maxCount: 1
  },
  {
    name:'passport',
    maxCount: 1
  },
  { 
    name: 'identificationfront', 
    maxCount: 1 
  },
  { 
    name: 'identificationback', 
    maxCount: 1 
  }
]
) ,Lawyercontroller.createlawyer);
lawyerroutes.post("/getallproviders", Lawyercontroller.getalllawyers);
lawyerroutes.post("/getallofflinecourses", Lawyercontroller.getallofflinecourses);
lawyerroutes.get("/getsingleofflinecourse/:id", Lawyercontroller.getsingleofflinecourse);
lawyerroutes.get("/getstudentsallpurchasedcourses/:id", Lawyercontroller.getstudentsallpurchasedcourses);
lawyerroutes.get("/getlawyerbyid/:id/:client_id", Lawyercontroller.getLawyerbyId);
lawyerroutes.get("/getlawyerbyidforlawyer/:id", Lawyercontroller.getLawyerbyIdbylawyer);
lawyerroutes.get("/getSingleProvider/:id", Lawyercontroller.getSingleProvider);
lawyerroutes.post("/updatelawyer",upload2.single('image'), Lawyercontroller.updatelawyer);
lawyerroutes.get(
  "/getlawyersbycabinetname/:cabinetname",
  Lawyercontroller.getlawyersbycabinetname
);
lawyerroutes.post("/updatecabinatelawyer",upload2.single('cabinet_lawyer_photo'), Lawyercontroller.updatecabinatelawyer);
lawyerroutes.get("/deletelawyer/:id", Lawyercontroller.deletelawyer);
lawyerroutes.get("/deletecabinatelawyer/:id", Lawyercontroller.deletecabinatelawyer);
lawyerroutes.post("/saveiabn", Lawyercontroller.saveiabn);
lawyerroutes.get("/deleteiabn/:lawyer_id", Lawyercontroller.deleteiabn);
lawyerroutes.get("/getiabn/:lawyer_id", Lawyercontroller.getiabn);

lawyerroutes.get("/getallnotificationsbyteacherid/:teacher_id", Lawyercontroller.getallnotificationsbyteacherid);


lawyerroutes.get("/getAllNotificationUserId/:user_id", Lawyercontroller.getAllNotificationUserId);

lawyerroutes.get("/getallnotificationsbystudentid/:student_id", Lawyercontroller.getallnotificationsbystudentid);
lawyerroutes.get("/getrevenue/:lawyer_id", Lawyercontroller.getrevenue);

lawyerroutes.get("/getbookeddates/:lawyer_id", Lawyercontroller.getbookeddates);
lawyerroutes.get("/getlawyersappointments/:lawyer_id",Lawyercontroller.getlawyersappointments);
lawyerroutes.get("/getalllawyersbycabinatename/:cabinate_name", Lawyercontroller.getalllawyersbycabinatename);
lawyerroutes.get("/getcurrentcommisionsetbyadmin",Lawyercontroller.getcurrentcommisionsetbyadmin);
lawyerroutes.post("/editlawyeraddress", Lawyercontroller.editlawyeraddress);
lawyerroutes.post("/deletelawyeraddress", Lawyercontroller.deletelawyeraddress);

lawyerroutes.get("/checkexpirydatesbysubscriptionID/:sub_id", Lawyercontroller.checkexpirydatesbysubscriptionID);
lawyerroutes.post("/checkifemailexists", Lawyercontroller.checkifemailexists);
lawyerroutes.get("/getNotificationPermission/:user_id", Lawyercontroller.getNotificationPermission);
lawyerroutes.post("/updatenotificationpermission", Lawyercontroller.updatenotificationpermission);
//provider

//service
lawyerroutes.post("/createservice",upload2.fields([
  { 
    name: 'image', 
    maxCount: 10
  }, 
  {
    name:'doc',
    maxCount: 10
  },
  {
    name:'content',
    maxCount: 10
  }
]
) , Servicescontroller.createservice);
lawyerroutes.get("/getallservicebyprovider/:lawyer_id", Servicescontroller.getallservicebyLayerId);
lawyerroutes.get("/getservicebyid/:id", Servicescontroller.getServicebyId);
lawyerroutes.post("/updateservice",upload2.fields([
  { 
    name: 'image', 
    maxCount: 10
  }, 
  {
    name:'doc',
    maxCount: 10
  },
  {
    name:'content',
    maxCount: 10
  }
]
) , Servicescontroller.updateService);
lawyerroutes.get("/deletecourse/:id",Servicescontroller.deletecourse);
//service


//ratings
lawyerroutes.post("/saveratings", Ratingcontroller.saveratingandreview);
lawyerroutes.get(
  "/getratingandreviewbyproviderid/:teacher_id",
  Ratingcontroller.getratingandreviewbylawyerid
);
lawyerroutes.get("/getallratingnreview", Ratingcontroller.getallratingnreview);
lawyerroutes.get(
  "/getratingandreviewbyclientid/:student_id",
  Ratingcontroller.getratingandreviewbyclientid
);
lawyerroutes.get(
  "/getratingandreviewbybyratingid/:rating_id",
  Ratingcontroller.getratingandreviewbybyratingid
);
lawyerroutes.post("/editratingandreview", Ratingcontroller.editratingandreview);
lawyerroutes.get("/deleteratingandreview/:rating_id", Ratingcontroller.deleteratingandreview);
//service

//miscs
lawyerroutes.get("/getlanguage", Misccontroller.getlanguage);
lawyerroutes.get("/getexpertise", Misccontroller.getexpertise);
//miscs

//banks
lawyerroutes.post("/savebankdetailsofprovider", LawyerBankDetailsController.savelawyerbankdetails);
lawyerroutes.post("/updatebankdetails", LawyerBankDetailsController.updatebankdetails);
lawyerroutes.get("/getteacherbankdetailsbyteacherid/:lawyer_id", LawyerBankDetailsController.getlawyerbankdetailsbylawyerid);
lawyerroutes.get("/deleteteacherbankdetails/:lawyer_id", LawyerBankDetailsController.deletelawyerbankdetails);
//banks




//bookings
lawyerroutes.get("/cancelbooking/:booking_id", Bookingscontroller.cancelbooking);
lawyerroutes.get("/acceptbooking/:booking_id/:lawyer_id", Bookingscontroller.acceptbooking);
lawyerroutes.get("/markserviceasdonebyprovider/:offer_id", Bookingscontroller.markserviceasdonebylawyer);
lawyerroutes.get("/markserviceasundonebylawyer/:offer_id", Bookingscontroller.markserviceasundonebylawyer);




//bookins

//schedule
lawyerroutes.post("/setSchedule", Schedulecontroller.setSchedule);
lawyerroutes.get("/getSchedulebyID/:provider_id", Schedulecontroller.getSchedulebyID);
lawyerroutes.post("/updateSchedule", Schedulecontroller.updateSchedule);
lawyerroutes.get("/deleteSchedule/:schedule_id", Schedulecontroller.deleteSchedule);
lawyerroutes.post("/setScheduleC", Schedulecontroller.setScheduleC);
lawyerroutes.get("/getSchedulebyIDC/:teacher_id", Schedulecontroller.getSchedulebyIDC);
lawyerroutes.post("/updateScheduleC", Schedulecontroller.updateScheduleC);
lawyerroutes.get("/deleteScheduleC/:schedule_id", Schedulecontroller.deleteScheduleC);
lawyerroutes.post("/setContactHours", Schedulecontroller.setContactHours);

lawyerroutes.post("/dateteSpecificTimeSloat", Schedulecontroller.dateteSpecificTimeSloat);
lawyerroutes.post("/updateSpecificDay", Schedulecontroller.updateSpecificDay);
lawyerroutes.get("/getContactHoursbyID/:provider_id", Schedulecontroller.getContactHoursbyID);
lawyerroutes.post("/updateContactHours", Schedulecontroller.updateContactHours);
lawyerroutes.get("/deleteContactHours/:day", Schedulecontroller.deleteContactHours);
//schedule




module.exports = lawyerroutes;