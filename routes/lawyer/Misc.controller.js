const languagemodel=require('../../models/admin/Languages');
const expertisemodel=require('../../models/admin/Expertise');
module.exports.getlanguage=async (req,res)=>{
    const language=await languagemodel.find({});
    res.json(language);
}
module.exports.getexpertise=async (req,res)=>{
    const expertise=await expertisemodel.find({});
    res.json(expertise);
}
