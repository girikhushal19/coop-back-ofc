const subscriptions = require("../../models/lawyer/Subscriptions");
const base_url = process.env.BASE_URL;
const {addmonthstodate}=require("../../modules/dates")
module.exports.createSubscription = async (req, res) => {
  try {
    const {
      subscriber_name,
      subscriber_email,
      type_of_subscription,
      name_of_package,
     
      duration,
      package_id,
    } = req.body;
    await subscriptions.find({
      subscriber_email: subscriber_email,
      type_of_subscription: type_of_subscription,
      payment_status: false
    }).then((result) => {
      console.log("result", result);
      if (result.length > 0) {
        console.log("result", result);
       if(result[0].payment_status==true){
        return res.send({
          status: false,
          message: "",
          errmessage:"L'abonnement existe déjà",
          data: result,
         
        });
       }else{
        const start_date=new Date().toISOString();
        const end_date=addmonthstodate(start_date,duration);
         const data={
          start_date:start_date,
          end_date:end_date
         }
         subscriptions.findByIdAndUpdate(result[0]._id, data, { new: true }).then((service) => {
         return  res.send({
            status: true,
            message: "Abonnement créé avec succès",
            data: result,
            errmessage:"",
            url:
              base_url +
              "/api/payment/" +
              req.body.payment_type +"lawyer"+
              "/" +
              service._id +
              "/" +
              req.body.amount,
          });
        });
       }
      } else {
        const start_date=new Date().toISOString();
        const end_date=addmonthstodate(start_date,duration);
    
        const subscription = new subscriptions();
        subscription.subscriber_name = subscriber_name;
        subscription.subscriber_email = subscriber_email;
        subscription.type_of_subscription = type_of_subscription;
        subscription.name_of_package = name_of_package;
        subscription.status = true;
        subscription.start_date = start_date;
        subscription.end_date = end_date;
        console.log(end_date)
        subscription.package_id = package_id;
        subscription.lawyer_id = req.body.lawyer_id;
        subscription.save().then((service) => {
          res.send({
            status: true,
            message: "Abonnement créé avec succès",
            data: result,
            errmessage:"",
            url:
              base_url +
              "/api/payment/" +
              req.body.payment_type +"lawyer"+
              "/" +
              service._id +
              "/" +
              req.body.amount,
          });
        });
      }
    });
  } catch (err) {
    console.log(err)
    res.send({
      status: false,
      message: "",
      errmessage:err.message,
      data: null,
    });
  }
};
module.exports.updateSubscription =async (req, res) => {
  const {
    subscriber_name,
    subscriber_email,
    type_of_subscription,
    name_of_package,
    status,
    start_date,
    end_date,
    subscription_id,
  } = req.body;
  const newenddate=new Date(end_date);
  const newstartdate=new Date(start_date);
 
  const data = {
    ...(subscriber_name && { subscriber_name }),
    ...(subscriber_email && { subscriber_email }),
    ...(type_of_subscription && { type_of_subscription }),
    ...(name_of_package && { name_of_package }),
    ...(status && { status }),
    ...(start_date && { start_date:newstartdate }),
    ...(end_date && { end_date:newenddate }),
  };
  console.log("data",data)
  const sub=await subscriptions.findById(subscription_id);
    sub.end_date=newenddate;
    sub.start_date=newstartdate;
    sub.payment_status=true;
    
    sub.save().then((result) => {
      res.send({
        status: true,
        message: "L'abonnement a été mis à jour avec succès",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "Abonnement non mis à jour",
        data: null,
        errmessage:err.message,
      });
    });
};
module.exports.getAllSubscriptions = (req, res) => {
  subscriptions.find({}).then((result) => {
    res.send({
      status: "success",
      message: "Subscriptions fetched successfully",
      data: result,
      errmessage:"",
    });
  });
};
module.exports.getSubscriptionById = (req, res) => {
  const subscription_id = req.params.subscription_id;
  subscriptions
    .findById(subscription_id)
    .then((result) => {
      res.send({
        status: "success",
        message: "Subscription fetched successfully",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: "error",
        message: "Subscription not fetched",
        data: err,
        errmessage:err.message,
      });
    });
};
module.exports.deleteSubscription = (req, res) => {
  const subscription_id = req.params.subscription_id;
  subscriptions
    .findByIdAndDelete(subscription_id)
    .then((result) => {
      res.send({
        status: true,
        message: "Abonnement supprimé avec succès",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "",
        data: null,
        errmessage:err.message,
      });
    });
};
module.exports.getSubscriptionbylawyerID = async(req, res) => {
  const lawyer_id = req.params.lawyer_id;
  await subscriptions
    .find({ lawyer_id:lawyer_id })
    .then((result) => {
      res.send({
        status: "success",
        message: "Subscriptions fetched successfully",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: "error",
        message: "",
        data: err,
        errmessage:err.message,
      });
    });
}
module.exports.getSubscriptionbylawyerIDred = async(req, res) => {
  const lawyer_id = req.params.lawyer_id;
  const subscription=await subscriptions.aggregate([
  { $match:{lawyer_id:lawyer_id}},
 
   {$project:{ type_of_subscription:1}}
  ]).then((result) => {
      res.send({
        status: "success",
        message: "Subscriptions fetched successfully",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: "error",
        message: "",
        data: err,
        errmessage:err.message,
      });
    });
}

