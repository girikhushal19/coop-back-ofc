const Ratingnreviews = require("../../models/client/Review");
const lawyermodel=require('../../models/lawyer/Lawunit');

const Servicesprovidedmodel=require("../../models/client/Servicesprovided");
module.exports.saveratingandreview = async(req, res) => {
  const provider_id = req.body.provider_id;
  const service_id = req.body.service_id;
  const provider_name = req.body.provider_name;
  const isjobdone=await Servicesprovidedmodel.findOne({_id:req.body.service_id});
  if(isjobdone.service_status)
  {
    let add_check = await Ratingnreviews.findOne({provider_id:provider_id,service_id:service_id});
    if(!add_check)
    {   
        if(!provider_name)
        {
            let law_rec = await lawyermodel.findOne({_id:provider_id},{first_name:1,last_name:1});
            if(law_rec)
            {
                provider_name = law_rec.first_name+" "+law_rec.last_name;
            }
        }
        const newrating = new Ratingnreviews();
        newrating.provider_id = provider_id;
        newrating.client_id = req.body.client_id;
    
        newrating.service_id=req.body.service_id
        newrating.service_name=req.body.service_name
        newrating.rating = req.body.rating;
        newrating.review = req.body.review;
    
        newrating.client_name=req.body.client_name;
        newrating.provider_name= provider_name;
        newrating.save().then((rating) => {
            return res.send({
                status: true,
                message: "La révision a été sauvegardée avec succès",
                data: {},
                errmessage: "",
            });
        
        }).catch((err) => {  
            return res.send({
                status: false,
                message: err.message,
                data: {},
                errmessage: "",
            });
         });
    }else{
        return res.send({
            status: false,
            message: "Note déjà attribuée à ce service",
            data: {},
            errmessage: "",
        });
    }
    
  }
  else{
    return res.send({
        status:false,
        message:"Le service n'est pas encore terminé attendez qu'il soit terminé"
    })
  }
};
module.exports.getratingandreviewbylawyerid = async(req, res) => {
    const provider_id = req.params.teacher_id;
   await Ratingnreviews.find({provider_id:provider_id}).then((rating) => {
        res.send({
            status: true,
            message: "La revue a été récupérée avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
      res.send({
        status: false,
        message: "",
        data: null,
        errmessage: "Erreur dans la récupération des avis ",
      });
    })
}
module.exports.getallratingnreview= (req, res) => {
    Ratingnreviews.find({}).then((rating) => {
        res.send({
            status: true,
            message: "Les coordonnées bancaires ont été sauvegardées avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la mise à jour de Bank Details",
        })
    })
}
module.exports.getratingandreviewbyclientid= async(req, res) => {
    const client_id=req.params.student_id;
   await Ratingnreviews.find({client_id:client_id}).then((rating) => {
        res.send({
            status: true,
            message: "Les coordonnées bancaires ont été sauvegardées avec succès",
            data: rating,
            errmessage: "",

        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la mise à jour de Bank Details",
        })
    })
}
module.exports.getratingandreviewbybyratingid= (req, res) => {
    const rating_id=req.params.rating_id;
    Ratingnreviews.find({rating_id:rating_id}).then((rating) => { 
        res.send(rating);
    }).catch((err) => {
        res.send(err);
    })
}
module.exports.deleteratingandreview= (req, res) => {
    const rating_id=req.params.rating_id;
    Ratingnreviews.findByIdAndRemove(rating_id).then((rating) => {
        res.send({
            status: true,
            message: "La révision a été supprimée avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la récupération des avis ",
        });
    })
}
module.exports.editratingandreview= (req, res) => {
    let {rating_id,rating,review}=req.body;
    const data={
        ...(rating&&{rating}),
        ...(review&&{review})
    }
    Ratingnreviews.findByIdAndUpdate(rating_id,data,{new:true}).then((rating) => {
        res.send({
            status: true,
            message: "La révision a été modifiée avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la récupération des avis ",
        });
    })
}
