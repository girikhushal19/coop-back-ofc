// utils
// const makeValidation = require('@withvoid/make-validation');
// models
const  { CHAT_ROOM_TYPES ,ChatRoom} = require('../../models/chat/ChatRoom');
const {ChatMessage} = require('../../models/chat/ChatMessage.js');
const UserModel = require('../../models/auth/user');
const GroupchatsModel = require('../../models/chat/GroupchatsModel');
const mongoose = require("mongoose");

module.exports=  {
  initiate: async (req, res) => {
    try {
      // const validation = makeValidation(types => ({
      //   payload: req.body,
      //   checks: {
      //     userIds: { 
      //       type: types.array, 
      //       options: { unique: true, empty: false, stringOnly: true } 
      //     },
      //     type: { type: types.enum, options: { enum: CHAT_ROOM_TYPES } },
      //   }
      // }));
      // if (!validation.success) return res.status(400).json({ ...validation });

      const { userIds, type } = req.body;
      const { chatInitiator  } = req.body;
      console.log("chatInitiator",chatInitiator)
      const allUserIds = [...userIds, chatInitiator];
      console.log("ChatRoomModel0",ChatRoom)
      const chatRoom = await ChatRoom.initiateChat(allUserIds, type, chatInitiator);
      return res.status(200).json({ success: true, chatRoom });
    } catch (error) {
      console.log(error)
      return res.status(200).json({ success: false, error: error })
    }
  },
  postMessage: async (req, res) => {
    try {
      const { roomId } = req.params;
      // const validation = makeValidation(types => ({
      //   payload: req.body,
      //   checks: {
      //     messageText: { type: types.string },
      //   }
      // }));
      // if (!validation.success) return res.status(400).json({ ...validation });

      const messagePayload = {
        messageText: req.body.messageText,
      };
      const currentLoggedUser = req.body.userId;
      const post = await ChatMessage.createPostInChatRoom(roomId, messagePayload, currentLoggedUser);
      global.io.sockets.in(roomId).emit('new message', { message: post });
      return res.status(200).json({ success: true, post });
    } catch (error) {
      console.log(error)
      return res.status(200).json({ success: false, error: error })
    }
  },
  getRecentConversation: async (req, res) => {
    try {
      const currentLoggedUser = req.params.userId;
      console.log("currentLoggedUser",currentLoggedUser)
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
      };
     
      const rooms = await ChatRoom.getChatRoomsByUserId(currentLoggedUser);
      // console.log("rooms",rooms)
      const roomIds = rooms.map(room => room._id);
      console.log("roomIds, options, currentLoggedUser",roomIds, options, currentLoggedUser)
      const recentConversation = await ChatMessage.getRecentConversation(
        roomIds, options, currentLoggedUser
      );
      console.log(recentConversation)
      return res.status(200).json({ success: true, conversation: recentConversation });
    } catch (error) {
      return res.status(200).json({ success: false, error: error })
    }
  },
  getConversationByRoomId: async (req, res) => {
    try {
      const { roomId } = req.params;
      console.log("roomId",roomId)
      const room = await ChatRoom.getChatRoomByRoomId(roomId)
      if (!room) {
        return res.status(400).json({
          success: false,
          message: 'No room exists for this id',
        })
      }
      const users = await UserModel.getUserByIds(room.userIds);
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
      };
      const conversation = await ChatMessage.getConversationByRoomId(roomId, options);
      return res.status(200).json({
        success: true,
        conversation,
        users,
      });
    } catch (error) {
      console.log(error)
      return res.status(200).json({ success: false, error });
    }
  },
  markConversationReadByRoomId: async (req, res) => {
    try {
      const { roomId } = req.params;
      const room = await ChatRoomModel.getChatRoomByRoomId(roomId)
      if (!room) {
        return res.status(400).json({
          success: false,
          message: 'No room exists for this id',
        })
      }

      const currentLoggedUser = req.userId;
      const result = await ChatMessageModel.markMessageRead(roomId, currentLoggedUser);
      return res.status(200).json({ success: true, data: result });
    } catch (error) {
      console.log(error);
      return res.status(200).json({ success: false, error });
    }
  },
  initiateChat:async(req,res)=>{
    try{
      //GroupchatsModel
      let user_id = [];
      let {ad_id,sender_id,message,user_type} = req.body;
      //room_id  ,,  user_id
      if(!sender_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'expéditeur est requis",
          errmessage:""
        })
      }
      if(!ad_id)
      {
        return res.send({
          status:false,
          message:"L'identité de la demande est requis",
          errmessage:""
        })
      }
      if(!message)
      {
        return res.send({
          status:false,
          message:"Le message est obligatoire",
          errmessage:""
        })
      }
      let ad_check = await GroupchatsModel.findOne({ad_id:ad_id});
      if(ad_check)
      {
        
        user_id = ad_check.user_id;
        let randomNumber = ad_check.room_id;
        if(!user_id.includes(sender_id))
        {
          user_id.push(sender_id);
        }
        //console.log("user_id ", user_id);
        await GroupchatsModel.create({
          user_id:user_id,
          user_type:user_type,
          room_id:randomNumber,
          ad_id:ad_id,
          sender_id:sender_id,
          message:message
        }).then((result)=>{
          return res.send({
            status:true,
            message:"Le chat initie pleinement le succès",
            errmessage:""
          })
        }).catch((e)=>{
          return res.send({
            status:false,
            message:"Quelque chose n'a pas fonctionné",
            errmessage:""
          })
        })
      }else{
        console.log("hereeeeeeeeeeeeeeeeee 189 line no......... ");
        var randomNumber = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');
        user_id.push(sender_id);
        await GroupchatsModel.create({
          user_id:user_id,
          user_type:user_type,
          room_id:randomNumber,
          ad_id:ad_id,
          sender_id:sender_id,
          message:message
        }).then((result)=>{
          return res.send({
            status:true,
            message:"Le chat initie pleinement le succès",
            errmessage:""
          })
        }).catch((e)=>{
          return res.send({
            status:false,
            message:"Quelque chose n'a pas fonctionné",
            errmessage:""
          })
        })
      }
    }catch(e)
    {
      return res.send({
        status:false,
        message:e.message,
        errmessage:""
      })
    }
  },
  getAllChatOfUser:async(req,res)=>
  {
    try{
      let {user_id} = req.body;
      await GroupchatsModel.aggregate([
        {
          $match:{
            sender_id:user_id
          }
        },
        {
          $group:{
            "_id":"$ad_id",
            "room_id":{$first:"$room_id"},
            "ad_id":{$first:"$ad_id"},
            "message":{$first:"$message"},
            "created_at":{$first:"$created_at"},
          }
        },
        {
          $sort:{
            "created_at":-1
          }
        }
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Succès",
          data:result
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message,
          errmessage:""
        })
      });
    }catch(e)
    {
      return res.send({
        status:false,
        message:e.message,
        errmessage:""
      })
    }
  },
  getSingleChatByRoomId:async(req,res)=>
  {
    try{
      //mongoose.set("debug",true);
      let {room_id} = req.params;
      await GroupchatsModel.aggregate([
        {
          $match:{
            room_id:room_id
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"usrId":{"$toObjectId":"$sender_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$usrId"]
                  }
                }
              }
            ],
            as:"user_record"
          }
        },
        {
          $sort:{
            "created_at":-1
          }
        },
        {
          $project:{
            "room_id":1,
            "ad_id":1,
            "sender_id":1,
            "message":1,
            "message":1,
            "created_at":1,
            "user_record.first_name":1,
            "user_record.last_name":1,
            "user_record.user_photo":1

          }
        }
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Succès",
          data:result
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message,
          errmessage:""
        })
      });
    }catch(e)
    {
      return res.send({
        status:false,
        message:e.message,
        errmessage:""
      })
    }
  },
  getSingleChatByAdId:async(req,res)=>
  {
    try{
      //mongoose.set("debug",true);
      let {ad_id} = req.params;
      await GroupchatsModel.aggregate([
        {
          $match:{
            ad_id:ad_id
          }
        },
        {
          $lookup:{
            from:"users",
            let:{"usrId":{"$toObjectId":"$sender_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$usrId"]
                  }
                }
              }
            ],
            as:"user_record"
          }
        },
        {
          $lookup:{
            from:"lawunits",
            let:{"slrId":{"$toObjectId":"$sender_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$slrId"]
                  }
                }
              }
            ],
            as:"seller_record"
          }
        },
        {
          $sort:{
            "created_at":-1
          }
        },
        {
          $project:{
            "room_id":1,
            "ad_id":1,
            "user_type":1,
            "sender_id":1,
            "message":1,
            "message":1,
            "created_at":1,
            "user_record.first_name":1,
            "user_record.last_name":1,
            "user_record.user_photo":1,
            "seller_record.first_name":1,
            "seller_record.last_name":1,
            "seller_record.photo":1

          }
        }
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Succès",
          allMsgCount:result.length,
          data:result
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message,
          errmessage:""
        })
      });
    }catch(e)
    {
      return res.send({
        status:false,
        message:e.message,
        errmessage:""
      })
    }
  }
}