const express =require('express');
// controllers
const chatRoom = require('./chatRoom.controller');

const router = express.Router();


router.get('/:userId', chatRoom.getRecentConversation);
router.get('/newroom/:roomId', chatRoom.getConversationByRoomId);
router.post('/initiate', chatRoom.initiate);
router.post('/:roomId/message', chatRoom.postMessage);
router.put('/:roomId/mark-read', chatRoom.markConversationReadByRoomId);
 
router.post("/initiateChat",chatRoom.initiateChat);
router.post("/getAllChatOfUser",chatRoom.getAllChatOfUser);
router.get("/getSingleChatByRoomId/:room_id",chatRoom.getSingleChatByRoomId);
router.get("/getSingleChatByAdId/:ad_id",chatRoom.getSingleChatByAdId);


module.exports=router;