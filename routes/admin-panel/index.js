const app = require("express");
const session = require("express-session");

const path = require("path");
const adminauth = require("../../middleware/adminauthtest");
const newAdminAuth = require("../../middleware/newAdminAuth");
const reasonModel=require("../../models/client/Creasons");
const checkifalreadylogin=require("../../middleware/checkifalreadylogin");
const multer = require("multer");
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const upload1 = multer({ dest: path.resolve(admin_user_profile_path) });
//const upload2 = multer({ dest: path.resolve(admin_lawyer_profile_path) });


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_lawyer_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload2 = multer({ storage: storage });


const AdminpanelController = require("./adminpanel.controller");
const adminpanelrouter = app.Router();
// adminpanelrouter.use(session({ secret: process.env.SESSIONSECRET,
//   resave: true,
//   saveUninitialized: true }));
adminpanelrouter.get("/", newAdminAuth,AdminpanelController.renderhomepage);
adminpanelrouter.get("/reservations/:reservation_id", adminauth, AdminpanelController.reservations);
adminpanelrouter.get("/users",newAdminAuth, AdminpanelController.users);
adminpanelrouter.get("/providers",newAdminAuth, AdminpanelController.lawyersindividual);
adminpanelrouter.get("/parents",adminauth, AdminpanelController.lawyerscabinat);
adminpanelrouter.get("/finishedreservations",newAdminAuth, AdminpanelController.servicehistory);
adminpanelrouter.get("/upcomingreservations", newAdminAuth,AdminpanelController.scheduledservices);
// adminpanelrouter.get("/upcomingreservations",adminauth, upcomingreservations);
// adminpanelrouter.get("/finishedreservations",adminauth, finishedreservations);
adminpanelrouter.get("/paymenthistory",newAdminAuth, AdminpanelController.paymenthistory);
adminpanelrouter.get("/getpages",AdminpanelController.getpages);
adminpanelrouter.get("/apptext_getpages",AdminpanelController.apptext_getpages);
adminpanelrouter.get("/paymentsettings",newAdminAuth, AdminpanelController.paymentsettings);
adminpanelrouter.get("/managelawyersincome",newAdminAuth, AdminpanelController.managelawyersincome);
adminpanelrouter.get("/managesubscriptionpackages",adminauth, AdminpanelController.managesubscriptionpackages);
adminpanelrouter.get("/managesubscriptions",adminauth, AdminpanelController.managesubscriptions);
adminpanelrouter.get("/carte", AdminpanelController.carte);
adminpanelrouter.get("/createnewsubpackage", adminauth,AdminpanelController.createnewsubpackage);
adminpanelrouter.post("/savesubscription",adminauth,AdminpanelController.savesubscription);
adminpanelrouter.get("/editsubscriptionpackage/:package_id",adminauth, AdminpanelController.Editsubscriptionpackage);
adminpanelrouter.get("/editsubscriptionpackage",adminauth, AdminpanelController.dummyfunction);
adminpanelrouter.post("/updatesubpackage",adminauth, AdminpanelController.updatesubscriptionpackage);
adminpanelrouter.get("/deletesubpackage/:package_id",adminauth, AdminpanelController.deletesubscriptionpackage);
adminpanelrouter.get("/viewsubpackage/:package_id",adminauth, AdminpanelController.viewsubscriptionpackage);
adminpanelrouter.get("/deleteuser/:user_id",adminauth, AdminpanelController.deleteuser);
adminpanelrouter.get("/viewuser/:user_id",adminauth, AdminpanelController.viewuser);
adminpanelrouter.get("/viewteacher/:lawyer_id",adminauth, AdminpanelController.viewlawyer);
adminpanelrouter.get("/deleteteacher/:lawyer_id",adminauth, AdminpanelController.deletelawyer);
adminpanelrouter.get("/activateteacher/:lawyer_id",adminauth, AdminpanelController.activatelawyer);
adminpanelrouter.get("/deactivatteacher/:lawyer_id",adminauth, AdminpanelController.deactivateawyer);
adminpanelrouter.get("/viewparent/:lawyer_id",adminauth, AdminpanelController.viewlawyercabinat);
adminpanelrouter.get("/deleteparent/:lawyer_id",adminauth, AdminpanelController.deletelawyercabinat);
adminpanelrouter.get("/activateparent/:lawyer_id",adminauth, AdminpanelController.activatelawyercabinat);
adminpanelrouter.get("/deactivateparent/:lawyer_id",adminauth, AdminpanelController.deactivateawyercabinat);
adminpanelrouter.post("/updatelawyer", adminauth
,upload2.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }, 
  {
    name:'fdl',
    maxCount: 10
  },
  {
    name:'bdl',
    maxCount: 10
  },
  {
    name:'passport',
    maxCount: 10
  },
  { 
    name: 'fid', 
    maxCount: 10 
  },
  { 
    name: 'bid', 
    maxCount: 10 
  }
]
)  ,AdminpanelController.updatelawyer);
adminpanelrouter.post("/updateparent",adminauth,upload2.array('files'), adminauth,AdminpanelController.updatelawyercabinat);
adminpanelrouter.post("/updateuser",adminauth,upload1.single('file') ,AdminpanelController.updateuser);
adminpanelrouter.post("/create_payment_setting", adminauth,AdminpanelController.create_payment_setting);
adminpanelrouter.get("/siteparameters", newAdminAuth,AdminpanelController.siteparameters);
adminpanelrouter.post("/savesiteparameters", adminauth,AdminpanelController.savesiteparameters);
adminpanelrouter.get("/logout", AdminpanelController.logout);
adminpanelrouter.get("/forgotpassowrd", AdminpanelController.forgotpassword);
adminpanelrouter.post("/forgotpassword", AdminpanelController.forgot_password);
adminpanelrouter.get("/reset_password",AdminpanelController.render_reset_password_template);
adminpanelrouter.post("/reset_password",AdminpanelController.reset_password);
adminpanelrouter.get("/cms",adminauth, AdminpanelController.cms);
adminpanelrouter.get("/login",checkifalreadylogin, AdminpanelController.login);
adminpanelrouter.post("/checklogin", AdminpanelController.checklogin);
adminpanelrouter.get("/register",AdminpanelController.register);
adminpanelrouter.post("/saveuser",AdminpanelController.saveuser);
adminpanelrouter.get("/notification",newAdminAuth, AdminpanelController.notification);
adminpanelrouter.post("/sendnotification",AdminpanelController.sendnotification);
adminpanelrouter.get("/viewnotifications",adminauth,AdminpanelController.viewnotifications);
adminpanelrouter.get("/getusers/:user_type",AdminpanelController.getusers);
// adminpanelrouter.get("/sendNotification",sendNotification);
adminpanelrouter.get("/viewpaymentdetails/:payment_id",AdminpanelController.viewpaymentdetails);
adminpanelrouter.post("/save_cms_page",AdminpanelController.save_cms_page);
adminpanelrouter.get("/listpages",adminauth,AdminpanelController.listpages);
adminpanelrouter.get("/editpage/:page_id",adminauth, AdminpanelController.editpage);
adminpanelrouter.post("/updatepage",adminauth, AdminpanelController.updatepage);
adminpanelrouter.get("/deletepage/:page_id",adminauth, AdminpanelController.deletepage);
adminpanelrouter.get("/language",newAdminAuth,AdminpanelController.language);
adminpanelrouter.get("/addlanguage",adminauth,AdminpanelController.addlanguage);
adminpanelrouter.get("/editlanguage/:language_id",adminauth,AdminpanelController.editlanguage);
adminpanelrouter.post("/updatelanguage",AdminpanelController.updatelanguage);
adminpanelrouter.get("/deletelanguage/:language_id",AdminpanelController.deletelanguage);
adminpanelrouter.post("/savelanguage",AdminpanelController.savelanguage);
adminpanelrouter.get("/getlanguage",AdminpanelController.getlanguage);
adminpanelrouter.get("/expertise",newAdminAuth,AdminpanelController.expertise);
adminpanelrouter.get("/addexpertise",adminauth,AdminpanelController.addexpertise);
adminpanelrouter.get("/editexpertise/:expertise_id",adminauth,AdminpanelController.editexpertise);
adminpanelrouter.post("/updateexpertise",AdminpanelController.updateexpertise);
adminpanelrouter.get("/deleteexpertise/:expertise_id",AdminpanelController.deleteexpertise);
adminpanelrouter.post("/saveexpertise",AdminpanelController.saveexpertise);
adminpanelrouter.get("/getexpertise",AdminpanelController.getexpertise);
adminpanelrouter.get("/setservicestatus/:serviceid",AdminpanelController.setservicestatus);
adminpanelrouter.get("/viewservicehistory/:service_id",AdminpanelController.viewservicehistory);
adminpanelrouter.get("/viewserviceschedule/:service_id",AdminpanelController.viewserviceschedule);
adminpanelrouter.get("/updatepaidamount/:id/:amount",AdminpanelController.updatepaidamount);
adminpanelrouter.get("/downloadinvoice/:id",AdminpanelController.downloadinvoice);
adminpanelrouter.get("/apptext",AdminpanelController.apptext);
adminpanelrouter.post("/apptext_save_cms_page",AdminpanelController.apptext_save_cms_page);
adminpanelrouter.get("/apptext_listpages",adminauth,AdminpanelController.apptext_listpages);
adminpanelrouter.get("/apptext_editpage/:page_id",adminauth, AdminpanelController.apptext_editpage);
adminpanelrouter.get("/comments", newAdminAuth,AdminpanelController.comments);
adminpanelrouter.post("/apptext_updatepage",adminauth, AdminpanelController.apptext_updatepage);
// adminpanelrouter.post("/emailcms",adminauth, emailcms);
adminpanelrouter.get("/apptext_deletepage/:page_id",adminauth, AdminpanelController.apptext_deletepage);

//reasons


adminpanelrouter.post("/createreason",AdminpanelController.createreason);
adminpanelrouter.post("/editreason",AdminpanelController.editreason);
adminpanelrouter.post("/deletereason",adminauth,AdminpanelController.deletereason);
adminpanelrouter.post("/viewreason",adminauth, AdminpanelController.viewreason);
adminpanelrouter.get("/getallreason",adminauth, AdminpanelController.getallreason);

adminpanelrouter.get("/addcatagoryonline",adminauth, AdminpanelController.addcatagoryonline);
adminpanelrouter.post("/savecatagoryonline",adminauth,upload2.fields([
  { 
    name: 'image', 
    maxCount: 10
  }
]
), AdminpanelController.savecatagoryonline);
adminpanelrouter.get("/editcatagoryonline/:id",adminauth,AdminpanelController.editcatagoryonline);
adminpanelrouter.post("/updatecatagoryonline",adminauth ,upload2.fields([
  { 
    name: 'image', 
    maxCount: 10
  }
]
) , AdminpanelController.updatecatagoryonline);
adminpanelrouter.get("/deletecatagoryonline/:id",adminauth, AdminpanelController.deletecatagoryonline);
adminpanelrouter.get("/managecatagoryonline",newAdminAuth, AdminpanelController.managecatagoryonline);
adminpanelrouter.get("/getallcatagories",adminauth, AdminpanelController.getallcatagories);
adminpanelrouter.get("/addcatagoryoffline",adminauth, AdminpanelController.addcatagoryoffline);
adminpanelrouter.get("/savecatagoryoffline",adminauth, AdminpanelController.savecatagoryoffline);
adminpanelrouter.get("/editcatagoryoffline",adminauth, AdminpanelController.editcatagoryoffline);
adminpanelrouter.get("/updatecatagoryoffline",adminauth, AdminpanelController.updatecatagoryoffline);
adminpanelrouter.get("/deletecatagoryoffline",adminauth, AdminpanelController.deletecatagoryoffline);
adminpanelrouter.get("/managecatagoryoffline",adminauth, AdminpanelController.managecatagoryoffline);

adminpanelrouter.get("/pendingonlinecourses",adminauth, AdminpanelController.pendingonlinecourses);
adminpanelrouter.get("/activatecourseonline/:id",adminauth, AdminpanelController.activatecourseonline);
adminpanelrouter.get("/deactivatecourseonline/:id",adminauth, AdminpanelController.deactivatecourseonline);
adminpanelrouter.get("/editcourseonline/:id",adminauth, AdminpanelController.editcourseonline);
adminpanelrouter.get("/approvecourseonline/:id",adminauth, AdminpanelController.approvecourseonline);
adminpanelrouter.post("/updatecourseonline",adminauth,upload2.fields([
  { 
    name: 'image', 
    maxCount: 10
  }, 
  {
    name:'doc',
    maxCount: 10
  }
]
), AdminpanelController.updatecourseonline);
adminpanelrouter.get("/deletecourseonline/:id",adminauth, AdminpanelController.deletecourseonline);

adminpanelrouter.get("/allcourses",adminauth, AdminpanelController.allcourses);
// adminpanelrouter.get("/upcomingreservations",adminauth, upcomingreservations);
// adminpanelrouter.get("/finishedreservations",adminauth, finishedreservations);
adminpanelrouter.get("/pendingofflinecourses",adminauth, AdminpanelController.pendingofflinecourses);
adminpanelrouter.get("/allofflinecourses",adminauth, AdminpanelController.allofflinecourses);
adminpanelrouter.get("/purchasehistory",adminauth, AdminpanelController.purchasehistory);
adminpanelrouter.get("/createcoupne",adminauth,AdminpanelController.createcoupne);
adminpanelrouter.get("/coupens",newAdminAuth, AdminpanelController.coupens);
adminpanelrouter.post("/savecoupens",adminauth, AdminpanelController.savecoupens);
adminpanelrouter.get("/editcoupen/:id",adminauth, AdminpanelController.editcoupen);
adminpanelrouter.get("/activatecoupen/:id",adminauth, AdminpanelController.activatecoupen);
adminpanelrouter.get("/deactivatecoupen/:id",adminauth, AdminpanelController.deactivatecoupen);
adminpanelrouter.post("/updatecoupen",adminauth, AdminpanelController.updatecoupen);
adminpanelrouter.get("/deletecoupen/:id",adminauth, AdminpanelController.deletecoupen);
adminpanelrouter.get("/createdistributer",adminauth, AdminpanelController.createdistributer);
adminpanelrouter.get("/distributers",newAdminAuth, AdminpanelController.distributers);
adminpanelrouter.post("/savedistributer",adminauth, AdminpanelController.savedistributer);
adminpanelrouter.get("/editdistributer/:id",adminauth, AdminpanelController.editdistributer);
adminpanelrouter.post("/updatedistributer",adminauth, AdminpanelController.updatedistributer);
adminpanelrouter.get("/deletedistributer/:id",adminauth, AdminpanelController.deletedistributer);
adminpanelrouter.post("/demandpayment",adminauth, AdminpanelController.demandpayment);
adminpanelrouter.get("/paymentrequests",adminauth, AdminpanelController.paymentrequests);
adminpanelrouter.get("/reasons",newAdminAuth, AdminpanelController.reasons);
adminpanelrouter.get("/addreason",adminauth, AdminpanelController.addreason);
adminpanelrouter.get("/acceptpaymentrequest/:id",adminauth, AdminpanelController.acceptpaymentrequest);
adminpanelrouter.get("/rejectpaymentrequest/:id",adminauth, AdminpanelController.rejectpaymentrequest);
adminpanelrouter.get("/updatereason/:id",adminauth, AdminpanelController.updatereason);
adminpanelrouter.get("/joblist",newAdminAuth, AdminpanelController.joblist);
adminpanelrouter.get("/portfoliohistory",newAdminAuth,AdminpanelController.portfoliohistory);
adminpanelrouter.get("/getalldemandpayment",adminauth, AdminpanelController.getalldemandpayment);
adminpanelrouter.get("/getdemandpaymentbyteacherid/:id",adminauth, AdminpanelController.getdemandpaymentbyteacherid);
module.exports = adminpanelrouter;

