const app = require("express");
const session = require("express-session");

const path = require("path");
const adminauth = require("../../middleware/adminauth");
const checkifalreadylogin=require("../../middleware/checkifalreadylogin");
const multer = require("multer");
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const upload1 = multer({ dest: path.resolve(admin_user_profile_path) });
const upload2 = multer({ dest: path.resolve(admin_lawyer_profile_path) });
const {
  getusers,
  sendnotification,
  viewnotifications,
  renderhomepage,
  users,
  lawyersindividual,
  lawyerscabinat,
  servicehistory,
  scheduledservices,
  paymenthistory,
  paymentsettings,
  managelawyersincome,
  managesubscriptionpackages,
  managesubscriptions,
  carte,
  createnewsubpackage,
  Editsubscriptionpackage,
  deletesubscriptionpackage,
  viewsubscriptionpackage,
  savesubscription,
  updatesubscriptionpackage,
  dummyfunction,
  deleteuser,viewuser,
  viewlawyer,
  deletelawyer,
  activatelawyer,
  deactivateawyer,
  updatelawyer,
  updateuser,
  create_payment_setting,
  siteparameters,
  logout,
  cms,
  savesiteparameters,
  login,
  checklogin,
  updatelawyercabinat,
  viewlawyercabinat,
  deletelawyercabinat,
  activatelawyercabinat,
  deactivateawyercabinat,
  register,
  saveuser,
  notification,
  expertise,
addexpertise,
editexpertise,
updateexpertise,
deleteexpertise,
saveexpertise,
getexpertise,
language,
addlanguage,
editlanguage,
updatelanguage,
deletelanguage,
savelanguage,
getlanguage,
// sendNotification,
viewpaymentdetails,
save_cms_page,
listpages,
editpage,
updatepage,
deletepage,
reservations,
viewservicehistory,
viewserviceschedule
} = require("./adminpanel.controller");
const adminpanelrouter = app.Router();
adminpanelrouter.use(session({ secret: process.env.SESSIONSECRET }));
adminpanelrouter.get("/",renderhomepage);
adminpanelrouter.get("/reservations/:reservation_id", reservations);
adminpanelrouter.get("/users", users);
adminpanelrouter.get("/lawyersindividual", lawyersindividual);
adminpanelrouter.get("/lawyerscabinat", lawyerscabinat);
adminpanelrouter.get("/servicehistory", servicehistory);
adminpanelrouter.get("/scheduledservices",scheduledservices);
adminpanelrouter.get("/paymenthistory", paymenthistory);
adminpanelrouter.get("/paymentsettings", paymentsettings);
adminpanelrouter.get("/managelawyersincome", managelawyersincome);
adminpanelrouter.get("/managesubscriptionpackages", managesubscriptionpackages);
adminpanelrouter.get("/managesubscriptions", managesubscriptions);
adminpanelrouter.get("/carte", carte);
adminpanelrouter.get("/createnewsubpackage",createnewsubpackage);
adminpanelrouter.post("/savesubscription",savesubscription);
adminpanelrouter.get("/editsubscriptionpackage/:package_id", Editsubscriptionpackage);
adminpanelrouter.get("/editsubscriptionpackage", dummyfunction);
adminpanelrouter.post("/updatesubpackage", updatesubscriptionpackage);
adminpanelrouter.get("/deletesubpackage/:package_id", deletesubscriptionpackage);
adminpanelrouter.get("/viewsubpackage/:package_id", viewsubscriptionpackage);
adminpanelrouter.get("/deleteuser/:user_id", deleteuser);
adminpanelrouter.get("/viewuser/:user_id", viewuser);
adminpanelrouter.get("/viewlawyer/:lawyer_id", viewlawyer);
adminpanelrouter.get("/deletelawyer/:lawyer_id", deletelawyer);
adminpanelrouter.get("/activatelawyer/:lawyer_id", activatelawyer);
adminpanelrouter.get("/deactivateawyer/:lawyer_id", deactivateawyer);
adminpanelrouter.get("/viewlawyercabinat/:lawyer_id", viewlawyercabinat);
adminpanelrouter.get("/deletelawyercabinat/:lawyer_id", deletelawyercabinat);
adminpanelrouter.get("/activatelawyercabinat/:lawyer_id", activatelawyercabinat);
adminpanelrouter.get("/deactivateawyercabinat/:lawyer_id", deactivateawyercabinat);
adminpanelrouter.post("/updatelawyer",upload2.array('files'),updatelawyer);
adminpanelrouter.post("/updatecabinetlawyer",upload2.array('files'),updatelawyercabinat);
adminpanelrouter.post("/updateuser",upload1.single('file') ,updateuser);
adminpanelrouter.post("/create_payment_setting",create_payment_setting);
adminpanelrouter.get("/siteparameters",siteparameters);
adminpanelrouter.post("/savesiteparameters",savesiteparameters);
adminpanelrouter.get("/logout", logout);
adminpanelrouter.get("/cms", cms);
adminpanelrouter.get("/login",checkifalreadylogin, login);
adminpanelrouter.post("/checklogin", checklogin);
adminpanelrouter.get("/register",register);
adminpanelrouter.post("/saveuser",saveuser);
adminpanelrouter.get("/notification", notification);
adminpanelrouter.post("/sendnotification",sendnotification);
adminpanelrouter.get("/viewnotifications",viewnotifications);
adminpanelrouter.get("/getusers/:user_type",getusers);
// adminpanelrouter.get("/sendNotification",sendNotification);
adminpanelrouter.get("/viewpaymentdetails/:payment_id",viewpaymentdetails);
adminpanelrouter.post("/save_cms_page",save_cms_page);
adminpanelrouter.get("/listpages",listpages);
adminpanelrouter.get("/editpage/:page_id", editpage);
adminpanelrouter.post("/updatepage", updatepage);
adminpanelrouter.get("/deletepage/:page_id", deletepage);
adminpanelrouter.get("/language",language);
adminpanelrouter.get("/addlanguage",addlanguage);
adminpanelrouter.get("/editlanguage/:language_id",editlanguage);
adminpanelrouter.post("/updatelanguage",updatelanguage);
adminpanelrouter.get("/deletelanguage/:language_id",deletelanguage);
adminpanelrouter.post("/savelanguage",savelanguage);
adminpanelrouter.get("/getlanguage",getlanguage);
adminpanelrouter.get("/expertise",expertise);
adminpanelrouter.get("/addexpertise",addexpertise);
adminpanelrouter.get("/editexpertise/:expertise_id",editexpertise);
adminpanelrouter.post("/updateexpertise",updateexpertise);
adminpanelrouter.get("/deleteexpertise/:expertise_id",deleteexpertise);
adminpanelrouter.post("/saveexpertise",saveexpertise);
adminpanelrouter.get("/getexpertise",getexpertise);
adminpanelrouter.get("/viewservicehistory/:service_id",viewservicehistory);
adminpanelrouter.get("/viewserviceschedule/:service_id",viewserviceschedule);
module.exports = adminpanelrouter;
