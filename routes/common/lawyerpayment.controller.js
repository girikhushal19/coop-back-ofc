const paypal = require('paypal-rest-sdk');
const User = require('../../models/auth/user');
const Notification = require('../../models/Notifications');
const lawunit = require('../../models/lawyer/Lawunit');
const Subscriptions = require('../../models/lawyer/Subscriptions');
const adminmodel = require('../../models/admin/Admin');
//var Policies = require('../../models/policies');
// const ComBackGround = require('../../models/communitybackground');
const stripe_key= process.env.STRIPE_SECRET_KEY_TEST;
const STRIPE_PUBLISHABLE_KEY_TEST = process.env.STRIPE_PUBLISHABLE_KEY_TEST;
const {savetransactionlawyer}=require('../admin/Transactions.controller');
const {sendpushnotificationtouser}=require('../../modules/Fcm');
const stripe = require('stripe')(stripe_key);
const key="pk_test_ZYZNOam8KTCxbRXa3RaBSeKk"
const YOUR_DOMAIN =process.env.BASE_URL;
module.exports.getPayPal =  async(req, res, next) => {
    var id = req.params.id;
    var amount = req.params.amount;
   
    var createPay = (payment) => {
        return new Promise((resolve, reject) => {
            paypal.payment.create(payment, function (err, payment) {
                if (err) {
                    reject(err);
                }
                else {
                    console.log("resolve"+resolve )
                    resolve(payment);
                }
            });
        });
    }
    paypal.configure({
        'mode': 'sandbox', //sandbox or live 
        'client_id': process.env.PAYPAL_ID,
        'client_secret': process.env.PAYPAL_SECRET    
    });
    var payment = {
        'intent': 'authorize',
        'payer': {
            'payment_method': 'paypal'
        },
        'redirect_urls': {
            'return_url': YOUR_DOMAIN+"/api/payment/paypallawyer/success/"+ id + "/" + amount+"/payment_status=true",
            'cancel_url': YOUR_DOMAIN+'/api/payment/paypallawyer/cancel/payment_status=false'
        },
        'transactions': [{
            'amount': {
                'total': amount,
                'currency': "USD"
            },
            'description': 'Slegal'
        }]
    }
    createPay(payment)
        .then((transaction) => {
            var id = transaction.id;
            var links = transaction.links;
            var counter = links.length;
            while (counter--) {
                if (links[counter].method == 'REDIRECT') {
                    return res.redirect(links[counter].href)
                }
            }
        })
        .catch(async(err) => {
            console.log(err);
            
            const service=Subscriptions.findOne({_id : id}, function (err, service) {
                if (err) {
                    console.log(err);
                    res.redirect("/api/payment/paypalfailedlawyer/payment_status=false");
                }
                else {
                    service.payment_status = false;
                    service.save(async(err, service) => {
                        if (err) {
                            console.log(err);
                            
                        }
                        else {
                            await savetransactionclient(service,err.message);
                            const user= await lawunit.findOne({_id : service.lawyer_id});
                            const message=process.env.MESSAGE_PAYMENT_FAILED;
                            sendpushnotificationtouser(message,user,service.lawyer_id)
                            console.log("service payment fialed saved successfully")
                        }
                    });
                }
            });
           
               
            res.redirect('/err');
        });

}

module.exports.getPayPalSuccess = async(req, res) => {
   try{
   
    console.log(req.params.id,req.params.amount);
    const admin= await adminmodel.findOne({});
    console.log(req.params.id)
   
    Subscriptions.findOne({_id : req.params.id}, function (err, user) {
        
        
        user.payment_amount = req.params.amount;
        user.transaction_id = req.query.paymentId;
        user.payment_status = true;
        user.date_of_transaction = new Date();
        user.mode_of_payment = "paypal";
        
        let type_of_subscription=user.type_of_subscription;
        user.save(async(err, service) => {
            if (err) {
                console.log(err);
                res.json({
                    success : false,
                    service : null,
                   
                    message : "erreur dans la sauvegarde du portefeuille."
                });
            }
            else {
            await savetransactionlawyer(service,"")
            const user= await lawunit.findOne({_id : service.lawyer_id});
            if(type_of_subscription=="En ligne" || type_of_subscription=="Online"|| type_of_subscription=="en ligne"|| type_of_subscription=="online"){
            user.modesofservice.online.isonline=true;
            }else if(type_of_subscription=="Hors ligne" || type_of_subscription=="Offline"|| type_of_subscription=="hors ligne"|| type_of_subscription=="offline"){
                user.modesofservice.offline.isoffline=true;
            }
            
            user.save().then(()=>{
            const message=process.env.MESSAGE_SUBSCRIPTION_SUCCESS+" "+user.first_name+" "+user.last_name;
            sendpushnotificationtouser(message,user,service.lawyer_id)
            
                   return res.json({
                            success : true,
                            service : service,

                            message : "paiement effectué"
                            });
            })
        }});
       
    });
   
}catch(err){
   
    res.send({
        success : false,
        service : null,
        message : err.message
    });
  }
}

module.exports.getPayPalCancel = (req, res) => {

    res.json({
    	success : false,
    	message : "paiement cancled"
    });

}

module.exports.editPolicy = (req, res) => {
//router.post('/edit-policy',function viewSettings(req, res, next) {
    console.log(req.body);
    console.log("req.body");
    console.log(req.file);
    console.log("req.files");
    Policies.findOne({_id : req.body.id }).sort({_id: -1}).exec(function geSettings(err, policies) {
        if(policies){
            console.log("in if")
            policies = policies;
            policies.Name = req.body.Name;
            if(req.file != null){
                if(req.file.size > 0){
                    policies.Image = policiesPath + req.file.filename;
                }
            }
            policies.commition =req.body.commition;
            policies.reccursion = req.body.reccursion;
            policies.status = true;
            policies.created_at = new Date();
            policies.save(function saveSetting(err) {
                if (err) {
                    console.log(err);
                }
                res.redirect('/admin/policies-list')
                   

                    });   
        }
        else{
            console.log("in if")
            var policies = new Policies();
            policies.Name = req.body.Name;
            policies.Image = policiesPath + req.file.filename;
            policies.commition =req.body.commition;
            policies.reccursion = req.body.reccursion;
            policies.status = true;
            policies.created_at = new Date();
            policies.save(function saveSetting(err) {
                if (err) {
                    console.log(err);
                }
                    
                    res.redirect('/admin/policies-list')

            });   

        }
        
    });


};

module.exports.editImage = (req, res) => {

ComBackGround.findOne({_id : req.body.id }).sort({_id: -1}).exec(function geSettings(err, images) {
        if(images){
            console.log("in if")
            images = images;
            images.UniqueId = Math.floor(100000 + Math.random() * 900000);
            if(req.file != null){
                if(req.file.size > 0){
                    //policies.Image = policiesPath + req.file.filename;
                        images.Link = policiesPath + req.file.filename;

                }
            }
            images.created_at = new Date();
            images.save(function saveSetting(err) {
                if (err) {
                    console.log(err);
                }
                res.redirect('/admin/ComBackGrounds-List')
                   

                    });   
        }
        else{
            console.log("in if")
            var images = new ComBackGround();
            images.UniqueId = Math.floor(100000 + Math.random() * 900000);
            images.Link = policiesPath + req.file.filename;
            images.created_at = new Date();
            images.save(function saveSetting(err) {
                if (err) {
                    console.log(err);
                }
                    
                    res.redirect('/admin/ComBackGrounds-List')

            });   

        }
        
    });

}

module.exports.getStripe =  (req, res, next) => {
    
    res.render('admin-panel/Stripe', { 
                key : STRIPE_PUBLISHABLE_KEY_TEST,
                amount : req.params.amount,
                currency : "eur",
                user_id : req.params.id,
                i18n : res,
                title : "Stripe",
                url:"/api/payment/postStripelawyer"
                

             }) 

}


module.exports.postStripe =  async(req, res, next) => {
   
    
    
   try{
    stripe.customers.create({ 
        email: req.body.stripeEmail, 
        source: req.body.stripeToken,
        name: req.body.first_name, 
        
        address: { 
            line1: 'test', 
            postal_code: 'test', 
            city: 'test', 
            state: 'test', 
            country: 'test', 
        } 
    })
    .then((customer) => { 
        console.log("customerid" , customer.id)
        return stripe.charges.create({ 
            amount: parseFloat(req.body.amount) * 100,     // Charing Rs 25 
            description: "making a service purchase", 
            customer:customer.id,
            currency: "eur"
           
        }); 
    }) 
    .then(async(charge) => { 
        console.log(charge.status)
        console.log("charge status")
        if(charge.status=="succeeded"){
                      
            let admin=await getmodel(adminmodel,{});
           
            Subscriptions.findOne({_id : req.body.id}, function (err, user) {
                user.payment_amount = req.body.amount;
                user.transaction_id = charge.id;
                user.payment_status =true;
                user.date_of_transaction = new Date().toISOString();
                user.mode_of_payment = "stripe";
                user.cardType = charge.payment_method_details.card.brand;
                user.cardNum = charge.payment_method_details.card.last4;
                let type_of_subscription=user.type_of_subscription;
                
                user.save(async(err, service) => {
                    if (err) {
                        console.log(err);
                        res.json({
                            success : false,
                            service : null,
                            url :YOUR_DOMAIN+"/api/payment/stripeFailedlawyer/payment_status=false",
                            message : "erreur dans la sauvegarde du portefeuille."
                        });
                    }
                    else {
                        await savetransactionlawyer(service,"");
                        console.log(service.lawyer_id)
                        const user= await lawunit.findOne({_id : service.lawyer_id});
                        console.log(type_of_subscription,"type_of_subscription")
                        if(type_of_subscription=="En ligne" || type_of_subscription=="Online"|| type_of_subscription=="en ligne"|| type_of_subscription=="online"){
                            user.modesofservice.online.isonline=true;
                            }else if(type_of_subscription=="Hors ligne" || type_of_subscription=="Offline"|| type_of_subscription=="hors ligne"|| type_of_subscription=="offline"){
                                user.modesofservice.offline.isoffline=true;
                            }
                            user.save(async(err, noservice) => {
                            if(err){
                                return res.send({
                                    status:false,
                                    message:"error",
                                    errmessage:"err",
                                    data:null
                                })
                            }
                            const message=process.env.MESSAGE_SUBSCRIPTION_SUCCESS;
                            console.log("message",message,"user",user,"id",service.lawyer_id)
                            sendpushnotificationtouser(message,user,service.lawyer_id)
                            return res.redirect("/api/payment/stripeSuccesslawyer/payment_status=true");
                            })
                    }
                });
            });
        
        }
        else{
            const service=Subscriptions.findOne({_id : req.params.id});
            await savetransactionclient(service,"payment failed");
            const user= await lawunit.findOne({_id : service.client_id});
            
            const message=process.env.MESSAGE_PAYMENT_FAILED;
          
            sendpushnotificationtouser(message,user,service.lawyer_id)
           
        res.redirect("/api/payment/stripeFailedlawyer/payment_status=false");

        }
        //res.send({code:200,succuss:true,charge:charge,message:"Wallet recharge successfully"})
       
    }) 
    .catch((err) => {
        console.log(err+err);
        res.send(err)       // If some error occurs 
    }); 
   }catch(err){

    console.log(err);
   }
}
module.exports.paypalfailedlawyer =  (req, res, next) => {
    res.render('admin-panel/paypalfailedlawyer', { 
                i18n : res,
                title : "Paypal failed",
                succuss : false
             }) 

}

module.exports.stripeSuccess =  (req, res, next) => {
    res.render('admin-panel/stripeSuccess', { 
                i18n : res,
                title : "Stripe succuss",
                succuss : true
             }) 

}

module.exports.stripeFailed =  (req, res, next) => {
    res.render('admin-panel/stripeFailed', { 
                i18n : res,
                title : "Stripe failed",
                succuss : false

             }) 

}
async function getmodel(model,filter){
 let response=await   model.findOne(filter);
    return response;
}