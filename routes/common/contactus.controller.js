const Contactus=require("../../models/admin/Cotactus");
const {sendmail}=require("../../modules/sendmail");
const path=require("path");
const client=require("../../models/auth/user");
const provider=require("../../models/lawyer/Lawunit");
module.exports.contactus=async(req,res)=>{
     const title=req.body.title;
     const reason=req.body.reason;
     const message=req.body.message;
     const id=req.body.id;
     const type=req.body.type;
     const name=req.body.name;
     const docs=[];
     let user;
     if(type=="user"){
     const clientv=await client.findById(id);
     if(clientv){
        user=clientv
     }else{
        return res.send({
            status:false,
            message:"user does not exists"
        })
     }
     }else if(type=="provider"){
        const providerv=await provider.findById(id);
        if(providerv){
           user=providerv
        }else{
           return res.send({
               status:false,
               message:"user does not exists"
           })
        }
     }else{
        return res.send({
            status:false,
            message:"type is required"
        })
     }
     const pathtofile = path.resolve("views/notifications/contactus.ejs");
     const subject="some weired things";
     
     if (req.files?.docs) {
      for(let i=0;i<req.files.docs.length;i++){
       let file="userprofile/"+req.files.docs[i].filename;
       docs.push(file)
      }
      }
     const newRequest=new Contactus();
     newRequest.title=title;
     newRequest.reason=reason;
     newRequest.message=message;
     newRequest.id=id;
     newRequest.name=name;
     newRequest.type=type;
     newRequest.docs=docs;
     newRequest.save().then(async(result)=>{
        
        sendmail(pathtofile, 
            { name: user.first_name+" "+user.last_name,message:message,reason:reason }, 
            subject,
            user.email);
     })
     return res.send({
        status:true,
        message:"messege sent"
     })

}