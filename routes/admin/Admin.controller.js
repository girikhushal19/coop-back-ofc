const User = require("../../models/auth/user");
const Lawyer = require("../../models/lawyer/Lawunit");
const Servicesprovided = require("../../models/client/Servicesprovided");
const Promotions = require("../../models/admin/Promotions");
const PaymentSettings = require("../../models/admin/Paymentsettings");
const Services = require("../../models/admin/Services");
const admincommision=require('../../models/admin/AdminCommision');
const getallusers = (req, res) => {
  User.find({})
    .then((users) => {
      res.send(users);
    })
    .catch((err) => {
      res.send(err);
    });
};
const getalllawyers = (req, res) => {
  Lawyer.find({})
    .then((lawyers) => {
      res.send(lawyers);
    })
    .catch((err) => {
      res.send(err);
    });
};
const serviceHistory = (req, res) => {
  Servicesprovided.find({
    date_availed: { $lt: new Date() },
  })
    .then((services) => {
      res.send(services);
    })
    .catch((err) => {
      res.send(err);
    });
};
const scheduledHistory = (req, res) => {
  Servicesprovided.find({
    date_availed: { $gte: new Date() },
  })
    .then((services) => {
      res.send(services);
    })
    .catch((err) => {
      res.send(err);
    });
};
const paymentSettings = (req, res) => {
  const paymentSettingsId = req.body.payment_settings_id;
  const paymentActive = req.body.active;
  PaymentSettings.findByIdAndUpdate(
    paymentSettingsId,
    { active: paymentActive },
    { new: true }
  )
    .then((paymentSettings) => {
      res.send(paymentSettings);
    })
    .catch((err) => {
      res.send(err);
    });
};
const createPaymentsettings = (req, res) => {
  const newPaymentSettings = new PaymentSettings();
  newPaymentSettings.name = req.body.name;
  newPaymentSettings.active = req.body.active;
  newPaymentSettings
    .save()
    .then((paymentSettings) => {
      res.send(paymentSettings);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
};
const getpaymentSettings = (req, res) => {
  PaymentSettings.find({})
    .then((paymentSettings) => {
      res.send(paymentSettings);
    })
    .catch((err) => {
      res.send(err);
    });
};
const createPromotions = (req, res) => {
  const newPromotion = new Promotions({
    Promotion_name: req.body.Promotion_name,
    Promotion_description: req.body.Promotion_description,
    promotion_expiry_date: req.body.promotion_expiry_date,
    promotion_type: req.body.promotion_type,
    promotion_amount: req.body.promotion_amount,
    promotion_code: req.body.promotion_code,
    promotion_percentage: req.body.promotion_percentage,
    promotion_status: req.body.promotion_status,
  });
  newPromotion
    .save()
    .then((promotion) => {
      res.send(promotion);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
};
const updatePromotions = (req, res) => {
  const promotionId = req.body.promotion_id;
  const {
    promotion_id,
    Promotion_name,
    Promotion_description,
    promotion_expiry_date,
    promotion_type,
    promotion_amount,
    promotion_percentage,
    promotion_status,
    promotion_code,
  } = req.body;
  const data = {
    ...(Promotion_name && { Promotion_name }),
    ...(Promotion_description && { Promotion_description }),
    ...(promotion_expiry_date && { promotion_expiry_date }),
    ...(promotion_type && { promotion_type }),
    ...(promotion_amount && { promotion_amount }),
    ...(promotion_percentage && { promotion_percentage }),
    ...(promotion_status && { promotion_status }),
    ...(promotion_code && { promotion_code }),
  };
  Promotions.findByIdAndUpdate(promotionId, data, { new: true })
    .then((promotion) => {
      res.send(promotion);
    })
    .catch((err) => {
      res.send(err);
    });
};
const deletePromotions = (req, res) => {
  const promotion_id = req.params.promotion_id;
  Promotions.findByIdAndDelete(promotion_id)
    .then((promotion) => {
      res.send(promotion);
    })
    .catch((err) => {
      res.send(err);
    });
};
const getallpromotions = (req, res) => {
  Promotions.find({})
    .then((promotions) => {
      res.send(promotions);
    })
    .catch((err) => {
      res.send(err);
    });
};
const getpromotionbyid = (req, res) => {
  Promotions.findById(req.params.promotion_id)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};
const createService = (req, res) => {
  const newService = new Services();
  newService.service_type = req.body.service_type;
  newService.service_description = req.body.service_description;
  newService
    .save()
    .then((service) => {
      res.send({ msg: "service created" });
    })
    .catch((err) => {
      res.send({ SUCCESS: "false" });
    });
};
const getallservices = (req, res) => {
  Services.find({})
    .then((services) => {
      res.send(services);
    })
    .catch((err) => {
      res.send(err);
    });
};
const getservicebyid = (req, res) => {
  Services.findById(req.params.service_id)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};
const updateservice = (req, res) => {
  
  const { service_id,service_type, service_description } = req.body;
  const data = {
    ...(service_type && { service_type }),
    ...(service_description && { service_description }),
  };
  Services.findByIdAndUpdate(service_id, data, { new: true })
    .then((service) => {
      res.send({ msg: "service updated" });
    })
    .catch((err) => {
      res.send({ msg: "service not updated" });
    });
};
const deleteservice = (req, res) => {
  const service_id = req.params.service_id;
  Services.findByIdAndDelete(service_id).then((service) => {
    res.send({ msg: "service deleted" });
  }).catch((err) => {
    res.send(err);
  }
)
  
}
const setAdminCommision = (req, res) => {
  const admin_percentage = req.body.admin_percentage;
  const admincomm=new admincommision();
  admincomm.commision=admin_percentage;
  admincomm.active=true;
  admincomm.save();
  admincommision.find({_id:{$ne:admincomm._id}}).then((result)=>{
    result.forEach(element => {
      element.active=false;
      element.save();
    }
    )
  }).catch((err)=>{
    res.send(err);
  });
  console.log(admincomm);
  res.send({msg:"admin commision set"});

}
const getAdminCommision = (req, res) => {
  admincommision.find({}).then((result)=>{
    res.send(result);
  }).catch((err)=>{
    res.send(err);
  }
  )
}
const deleteadmincommision = (req, res) => {
  const admin_percentage = req.body.admin_percentage;
  admincommision.findByIdAndDelete(admin_percentage).then((result)=>{
    res.send({msg:"admin commision deleted"});
  }).catch((err)=>{
    res.send(err);
  }
  )
}
const setactiveadmincommision = (req, res) => {
  const admin_id= req.body.id;
  admincommision.findByIdAndUpdate(admin_id,{active:true}).then((result)=>{
    admincommision.find({_id:{$ne:admin_id}}).then((result)=>{
      result.forEach(element => {
        element.active=false;
        element.save();
      }
      )
    }).catch((err)=>{
      res.send(err);
    }
    )
    res.send({msg:"admin commision set"});
  }).catch((err)=>{
    res.send(err);
  }
  )
}

module.exports = {
  serviceHistory,
  scheduledHistory,
  getallusers,
  getalllawyers,
  createPromotions,
  updatePromotions,
  deletePromotions,
  getallpromotions,
  getpromotionbyid,
  paymentSettings,
  paymentSettings,
  createPaymentsettings,
  getpaymentSettings,
  updateservice,
  getservicebyid,
  getallservices,
  createService,
  deleteservice,
  setAdminCommision,
  setactiveadmincommision,
  deleteadmincommision,
  getAdminCommision
};
