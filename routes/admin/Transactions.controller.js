const transactionsmodel = require("../../models/admin/Transactions");
const savetransactionclient = (service,remark) => {
   let payer_id=service.student_id;
    let payment_amount=service.payment_amount;
    let payment_type=service.mode_of_payment;
    let payment_transaction_id=service.transaction_id;
    let payment_status=service.payment_status;
    let payment_date=service.date_of_transaction;
    let remarks=remark
    const data={
        ...(payer_id && {payer_id}),
        ...(payment_amount && {payment_amount}),
        ...(payment_type && {payment_type}),
        ...(payment_transaction_id && {payment_transaction_id}),
        ...(payment_status && {payment_status}),
        ...(payment_date && {payment_date}),
        ...(remarks && {remarks})
    }
    const newTransaction = new transactionsmodel(data);
    newTransaction.save((err, transaction) => {
       if(err){
           return false
       }
       return true;
    })
}

const savetransactionlawyer = (service,remark) => {
    let payer_id=service.teacher_id;
    let payment_amount=service.payment_amount;
    let payment_type=service.mode_of_payment;
    let payment_transaction_id=service.transaction_id;
    let payment_status=service.payment_status;
    let payment_date=service.date_of_transaction;
    let remarks=remark
    const data={
        ...(payer_id && {payer_id}),
        ...(payment_amount && {payment_amount}),
        ...(payment_type && {payment_type}),
        ...(payment_transaction_id&& {transaction_id:payment_transaction_id}),
        ...(payment_status && {payment_status}),
        ...(payment_date && {payment_date}),
        ...(remarks && {remarks})

}
const newTransaction = new transactionsmodel(data);
newTransaction.save((err, transaction) => {
     if(err){
            return false
        }
        return true;
    }
)
}
const getalltransactions = (req, res) => {
    transactionsmodel.find({}, (err, transactions) => {
        if (err) {
            res.json({
                success: false,
                message: err
            })
        } else {
            res.json({
                success: true,
                transactions: transactions
            })
        }
    })
}
const gettransactionbyid = async(req, res) => {
    const id=req.params.id;
  await  transactionsmodel.find({
        payer_id:id
    }).then(transactions => {
        if (transactions) {
            res.json({
                success: true,
                transactions: transactions
            })
        } else {
            res.json({
                success: false,
                message: "No transactions found"
            })
        }
    }).catch(err => {
        res.json({
            success: false,
            message: err.message
        })
    })
}
module.exports = {
    savetransactionclient,
    savetransactionlawyer,
    getalltransactions,
    gettransactionbyid
    
}