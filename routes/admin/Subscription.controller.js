const Subscription = require("../../models/admin/SubscriptionPackages");
const createSubscription = (req, res) => {
  const { package, price, duration, description } = req.body;
  const subscription = new Subscription({
    package,
    price,
    duration,
    description,
  });
  subscription
    .save()
    .then((result) => {
      res.send({"status": "success", "message": "Subscription created successfully", "data": result});
    })
    .catch((err) => {
      res.send(err);
    });
};
const updateSubscription = (req, res) => {
  const { package, price, duration, description, subscription_id } = req.body;
  Subscription
    .findByIdAndUpdate(subscription_id, {
      ...(package && { package }),
      ...(price && { price }),
      ...(duration && { duration }),
      ...(description && { description }),
    })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
};
const getAllSubscriptions = (req, res) => {
  Subscription
        .find({})
        .then((result) => {
        res.send(result);
        })
        .catch((err) => {
        res.send(err);
        });
}
const getSubscriptionById = (req, res) => {
    const subscription_id  = req.params.subscription_id;
    Subscription
        .findById(subscription_id)
        .then((result) => {
        res.send(result);
        })
        .catch((err) => {
        res.send(err);
        });
}
const deleteSubscription = (req, res) => {
    const subscription_id  = req.params.subscription_id;
    Subscription
        .findByIdAndDelete(subscription_id)
        .then((result) => {
        res.send(result);
        })
        .catch((err) => {
        res.send(err);
        });
}
module.exports={createSubscription,updateSubscription,getAllSubscriptions,getSubscriptionById,deleteSubscription};