const mongoose = require('mongoose');
const ExpertiseSchema=mongoose.Schema({
    name:{type:String,required:true},
    type:{type:String,default:'Expertise'},
})
module.exports=mongoose.model("Expertise",ExpertiseSchema);