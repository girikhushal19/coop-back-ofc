const mongoose=require("mongoose");
const PaymentRequestSchema=mongoose.Schema({
    bank_id: { type: String,defaultL:"" },
    provider_id:{type:String,required:true},
    email:{type:String,required:true},
    provider_name:{type:String,required:true},
    request_amount:{type:Number,required:true},
    request_date:{type:Date,default:new Date()},
    payment_status:{type:Boolean,default:false},
    is_rejected:{type:Boolean,default:false},
    created_at:{type:Date,default:Date.now}
});
module.exports=mongoose.model("PaymentRequest",PaymentRequestSchema);