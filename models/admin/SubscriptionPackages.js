const mongoose=require('mongoose');
const SubscriptionPakagesSchema=new mongoose.Schema({
   
    package:{type:String,required:true},
    price:{type:Number,required:true},
    duration:{type:Number,required:true},
    description:{type:String,required:true},
    type:{type:String,required:true},
    

});
module.exports=mongoose.model('SubscriptionPackages',SubscriptionPakagesSchema);