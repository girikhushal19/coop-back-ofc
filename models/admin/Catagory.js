const mongoose=require("mongoose");
const CatagorySchema=mongoose.Schema({
   mastercatagory:{type:mongoose.Types.ObjectId},
   title:{type:String,unique:true},
   image:{type:String},
   is_parent:{type:Boolean},
   price:{type:Number}
});
module.exports=mongoose.model("Catagory",CatagorySchema);