const mongoose = require('mongoose');
const NotificationSchema = new mongoose.Schema({
    to_id: {type:String},
    message: {type:String},
    status: {type:String},
    notification_type: {type:String},
    date: {type:Date},
    from_email: {type:String},
})
module.exports = mongoose.model('Notification', NotificationSchema);