const mongoose=require("mongoose");
const groupchatsSchema=new mongoose.Schema({
    user_type:{type:Number,default:1}, // 1 user 2 provider
    user_id:{type:Array,default:[]},
    room_id:{type:String,default:""},
    ad_id:{type:String,default:""},
    sender_id:{type:String,default:""}, 
    message:{type:String,default:""},
    created_at:{type:Date,default: Date.now},
    updated_at:{type:Date,default: Date.now},
} 
);


module.exports=mongoose.model("groupchats",groupchatsSchema);