var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChatSchema = new Schema({
    //senderID: {type: Schema.Types.ObjectId, ref: 'User'},
    //senderName: {type: String},
    //senderProfile: {type: String},
    //receiverID: {type: Schema.Types.ObjectId, ref: 'User'},
    //receiverName: {type: String},
    //receiverProfile: {type: String},
    //productID : {type: Schema.Types.ObjectId, ref: 'Product'},
    //productName : {type: String},
    //createdAt: {type: Date},
    //status: {type: String},
    //message : {type: String},
    //byMe : {type: String},
    //name : {type: String},
    roomId :  {type: String},
    users : {type : Array},
    chats : {type : Array},
    //countSender : {type : Number},
    count : {type : Number, default : 0},
    statusEventOne : {type : String ,default : null},
    statusEventTwo : {type : String ,default : null},
    statusDateFinal : {type : String ,default : null},
    status : {type : Boolean, default : false},
    updated_at: {
        type: Date
    }
    
});


module.exports = mongoose.model('chat', ChatSchema);
