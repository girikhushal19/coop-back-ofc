const mongoose = require('mongoose');
const NotificationPermissionSchema = new mongoose.Schema({
    id: {type:String, required:true},
    user_type:{type:String},
    email_reminder:{type:Boolean,default:true},
    push_reminder:{type:Boolean,default:true},
    sms_reminder:{type:Boolean,default:true},
    email_reservation:{type:Boolean,default:true},
    push_reservation:{type:Boolean,default:true},
    sms_reservation:{type:Boolean,default:true},
    
    email_offer:{type:Boolean,default:true},
    push_offer:{type:Boolean,default:true},
    sms_offer:{type:Boolean,default:true},

    np_g_gd:{type:Boolean,default:true}, // new product , gift , good deals
    coop_post:{type:Boolean,default:true}, // post from coop
    promo_code:{type:Boolean,default:true},
    provider_message:{type:Boolean,default:true},

})
module.exports = mongoose.model('NotificationPermission', NotificationPermissionSchema);