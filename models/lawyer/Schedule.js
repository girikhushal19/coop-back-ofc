const mongoose = require("mongoose");
const ScheduleSchema = new mongoose.Schema({
  provider_id: { type: String, required: true },
  
  date: { type: Date, required: true },
  time: { type: String, required: true }
});
 module.exports = mongoose.model("Schedule", ScheduleSchema);