const mongoose = require("mongoose");
const IabnSchema = new mongoose.Schema({
    lawyer_id: { type: String, required: true },
    iabn: { type: String, required: true },
});
module.exports = mongoose.model("Iabn", IabnSchema);