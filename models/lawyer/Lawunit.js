const mongoose = require("mongoose");

const LawunitSchema = new mongoose.Schema({
  photo: { type: String, default: "" },
  first_name: { type: String ,default: ""},
  last_name:{ type: String, default: ""},
  email: { type: String, unique: true ,default: ""},
  password: { type: String,default: "" },
  country: { type: String,default: "" },
  pin:{ type: String,default: "" },
  proffession:{ type: String,default: "" },
  sex:{ type: String,default: "" },
  description:{ type: String,default: "" },
  address:{type:String,default:""},
  location_cor:{ 
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        default:[]
    }
  },
  // location: {
  //   type: { type: String },
  //   coordinates: []
  //  },
  phone: { type: String ,default: ""},
  dob: { type: String ,default: ""},
  avalible_instant:{type:Boolean,default:false},
  
  is_deleted: { type: Boolean, default: false },
  is_active: { type: Boolean,Default: true },
  token: { type: String },
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  },
  fcm_token:{type:String,default:""},
  totalpaidamount:{type:Number,default:0},
  totalpendingamount:{type:Number,default:0},
  extProvider:{type:Boolean,default:false},
  identifications:{front:{type:String,default:""},back:{type:String,default:""}},
  passport:{type:String,default:""},
  drivinglicence:{front:{type:String,default:""},back:{type:String,default:""} },
 
},{ timestamps: true });
LawunitSchema.index({"location_cor": '2dsphere'});
//LawunitSchema.index({"location": '2dsphere'});
module.exports = mongoose.model("Lawunit", LawunitSchema);
