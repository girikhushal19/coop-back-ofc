const mongoose = require("mongoose");
const ContactHoursSchema = new mongoose.Schema({
  provider_id: { type: String, required: true },
  dayNumber:{type:Number,required:true},
  day: { type: String, required: true },
  // time_availible: { type: String, required: true },
  opening_hour:{ type: String },
  closing_hour:{ type: String },
  opening_hour_num:{ type: Number },
  closing_hour_num:{ type: Number }
});
 module.exports = mongoose.model("ContactHours", ContactHoursSchema);