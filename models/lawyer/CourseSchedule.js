const mongoose = require("mongoose");
const CourseScheduleSchema = new mongoose.Schema({
    course_id: { type: String, required: true },
    teacher_id:{ type: String, required: true },
    date: { type: Date, required: true },
    time:{type:String ,required:true}
});
module.exports = mongoose.model("CourseSchedule", CourseScheduleSchema);