const mongoose = require("mongoose");
const ServicesSchema = new mongoose.Schema({
    provider_id: { type: String, required: true },
    provider_name: { type: String, required: true },
    catagory: { type: String, required: true },
    catagory_id: { type: String, required: true },
    title:{ type: String, required: true },
   
    experience: { type: String ,default: ""},
    desc:{ type: String, required: true },
    typeofjobs:{ type: String },
   
    is_approved:{ type: Boolean,default:true },
    is_active:{type:Boolean,default:true},
    diplomaname: { type: String ,default: ""},
    
    image:{ type: String },
    price:{ type: Number, required: true }
   
   
    
   
},
{
    timestamps:true
}
);
module.exports = mongoose.model("LawyerServices", ServicesSchema);