const mongoose=require("mongoose");
const Servicesprovided=new mongoose.Schema({
   
    
    client_id:{type:String},
    client_name:{type:String},
    provider_id:{type:String},
    provider_name:{type:String},
    service_id:{type:String},
    service_name:{type:String},
    catagory_id:{type:String},
    catagory:{type:String},
    date_availed:{type:Date},
    time_availed:{type:String},
    title:{type:String},
    service_status:{type:Boolean,default:false},
    payment_status:{type:Boolean,default:false},
    payment_amount:{type:Number},
    mode_of_payment:{type:String},
    address:{type:String,default:""},
    location:{type:String,default:""},
    location_cor:{ 
        type: {
            type: String,
            default: "Point"
        },
        coordinates: {
            type: [Number],
            default:[0.0000,0.0000]
        }
      },
    commission_earned:{type:Number},
    date_of_transaction:{type:Date},
    transaction_id:{type:String},
    docs:{type:Array},
    service_acceptance_status_by_provider:{type:Boolean,default:false},
    service_acceptance_status_by_client:{type:Boolean,default:false},
    cancelation_reason_by_user:{type:String},
    cancelation_message_by_user:{type:String},
    cardType:{type:String,default:""},
    cardNum:{type:String,default:""},
    cancelation_reason_by_provider:{type:String},
    cancelation_message_by_provider:{type:String},
    is_service_rejected:{type:Boolean,default:false},
    promo:{type:String},
    duration:{type:String},
    duration_2:{type:Number,default:0},
    desc:{type:String},
    contact:{type:String},
    finalamount:{type:Number},
    
    currency:{type:String,default:"CHF"},
    payment_failed:{type:Boolean,default:false},
    created_at:{type:Date,default:Date.now}
});
Servicesprovided.index({"location_cor": '2dsphere'});
module.exports=mongoose.model("Servicesprovided",Servicesprovided);