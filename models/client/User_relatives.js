const mongoose = require("mongoose");

const User_relativesSchema = new mongoose.Schema({
  
  first_name: { type: String,required:true },
  address: { type: String,required:true },
  title: { type: String,required:true },
  dob:{type:String},
  date_of_registration:{type:Date,required:true},
  user_id:{type:String,required:true},
});



module.exports = mongoose.model("User_relatives", User_relativesSchema);