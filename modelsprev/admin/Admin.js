const mongoose=require("mongoose");
const AdminSchema=mongoose.Schema({
   
    email:{type:String,required:true},
    password:{type:String,required:true},
    commissions:{type:Number,default:0},
    token:{type:String},
});
module.exports=mongoose.model("Admin",AdminSchema);