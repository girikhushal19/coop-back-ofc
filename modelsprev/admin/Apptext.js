const mongoose=require("mongoose");
const ApptextSchema=mongoose.Schema({
   
   page_name:{type:String,required:true},
    page_content:{type:String,required:true},
    page_heading:{type:String}
});
module.exports=mongoose.model("Apptext",ApptextSchema);