const mongoose = require("mongoose");
const ContactHoursSchema = new mongoose.Schema({
  lawyer_id: { type: String, required: true },
  
  day: { type: String, required: true },
  time_availible: { type: String, required: true },
});
 module.exports = mongoose.model("ContactHours", ContactHoursSchema);