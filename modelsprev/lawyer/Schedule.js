const mongoose = require("mongoose");
const ScheduleSchema = new mongoose.Schema({
  lawyer_id: { type: String, required: true },
  
  date: { type: Date, required: true },
  modesofservice: {
    online: { time_availible: { type: String } },
    offline: { time_availible: { type: String } },
  },
});
 module.exports = mongoose.model("Schedule", ScheduleSchema);