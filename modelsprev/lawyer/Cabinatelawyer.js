const mongoose = require("mongoose");

const CabinatelawyerSchema = new mongoose.Schema({
 
 photo:{type:String,default:""},
  cabinetname: { type: String },
  first_name: { type: String },
  last_name:{ type: String },
  expertise_in: { type: String },
location_serving:{type:String},
is_deleted: { type: Boolean, default: false },
is_active: { type: Boolean, default: true },
created_at: { type: Date, default: new Date().toISOString() },

});

module.exports = mongoose.model("Cabinatelawyer", CabinatelawyerSchema);
