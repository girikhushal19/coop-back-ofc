const mongoose = require("mongoose");
const ServicesSchema = new mongoose.Schema({
    lawyer_id: { type: String, required: true },
    
    service_id: { type: String, required: true },
    modesofservice:{
        online:{isonline:{type:String,required:true},price:{type:String,required:true}},
        offline:{isoffline:{type:String,required:true},price:{type:String,required:true}},
    },
});
module.exports = mongoose.model("LawyerServices", ServicesSchema);