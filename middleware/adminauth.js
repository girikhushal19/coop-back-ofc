const jwt = require("jsonwebtoken");
const adminmodel = require("../models/admin/Admin");
const config = process.env;
// var LocalStorage = require('node-localstorage').LocalStorage;
//   localStorage = new LocalStorage('./scratch');
const verifyToken = async(req, res, next) => {
  // var token =localStorage.getItem("token");
  var token=req.session.user_id;
  if (!token) {
  
    console.log("token not found");
    console.log(token);
    //return res.redirect("/admin-panel/login");
  }
  // try {
  //     const newtoken=token;
  //   const decoded = jwt.verify(newtoken, process.env.TOKEN_KEY);
  //   req.user = decoded;
  // } catch (err) {
  //   console.log("err");
  //   console.log(err);
  //   return res.redirect("/admin-panel/login");
  // }
  return next();
};

module.exports = verifyToken;   