const http = require("http");
const app = require("./app");
const server = http.createServer(app);
var Chat = require('./models/chat/chat')
const Notification = require('./models/Notifications')
var User = require('./models/auth/user');
const socketio = require("socket.io");
const WebSockets =require("./utils/WebSockets.js");
const io = socketio(server);
// io.on("connection", WebSockets.connection)
// console.log("io",io)
const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;
// module.exports={io}
// server listening 
// global.io = socketio.listen(server);
console.log("port ", port);
//return false;

app.use((req, res, next) => {
  req.io = io;
  return next();
});
io.on('connection', (socket) => {
  console.log('a user connected');
  // socket.on('sendMessage', (message) => {
  //   console.log(message);

  //   io.emit('getMessage', `${socket.id.substr(0, 2)}: ${message}`);
  // });
  socket.on('disconnect', () => {
    console.log('a user disconnected!');
  });
});
const ss = require("./routes/chat/TestSocketController")(io);

server.listen(port,function () {
  console.log(`Server listening on port ${port}`);
 
 });

 

/*
 io.on('connection', socket => {
  console.log("in connection ......")
       socket.on('chat', data => {
            let message = data.message;
            let recieverId = data.recieverId;
            let receiverName = data.receiverName;
            let receiverProfile = data.receiverProfile;
            let senderId = data.senderId;
            let senderName = data.senderName;
            let senderProfile = data.senderProfile;
            var count = 0;
            var roomId1 = data.roomID;
            var roomId2 = data.roomID;
            var room=data.roomID;
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var dateTime = today;
                Chat.findOne({
                  roomId: roomId1
                  }).exec(function getTabacs(err, roomDetails) {
                    
                    // console.log("roomDetails",roomDetails)
                  
                  if(roomDetails){
                   let isnewsender= !roomDetails?.users.find(e=>e==data.senderId);
                   if(isnewsender){
                    roomDetails.users.push(data.senderId);
                   }
                   console.log("isnewsender",isnewsender,data.senderId)
                        roomDetails.chats.unshift({ 
                          message: data.message, 
                          by: data.senderId,
                          senderId : data.senderId,
                          senderName : data.senderName,
                          senderProfile : data.senderProfile,
                          recieverId : data.recieverId,
                          receiverName : data.receiverName,
                          receiverProfile : data.receiverProfile,
                          time : dateTime 
                        });
                        if(roomDetails.status == true){
                         console.log(roomDetails.count)
                         console.log("roomDetails.count")
                         count = roomDetails.count + 1 ;  
                        }
                        else {
                         count = 1;  
                        }
                        roomDetails.status = true;
                        roomDetails.count = count;
                        roomDetails.updated_at = new Date();  
                        roomDetails.save().then(() => {
                        var notification = new Notification();
                        notification.createdAt = new Date();
                        notification.adminID = recieverId;
                        notification.status = 'unread';
                        notification.details = '';
                        notification.title = 'Vous avez un nouveau message de' + data.name ;
                        notification.sentTo = recieverId;
                        notification.sentBy =  senderId;
                        notification.type = 'Message';
                  
                        notification.save(function(err) {
                          if (err) {
                            console.log(err);
                          }
                          User.findById(recieverId, function getUser(err, user1) {
                              if (err) {
                                  console.log(err);
                                  console.log("in error of user",err);
                                  errorMessage = err;
                              } else {
                                  if (user1?.device_token != undefined || user1?.device_token != null) {
                                      // sendPushNotifications(user1, notification.title, notification.type, user1.device_token);
                                    io.sockets.emit(room, roomDetails);
                                  }
                                  io.sockets.emit(room, roomDetails);
                              }
                          });
                      })
                      });
                  } else {
                    var roomDetails = new Chat();
                    roomDetails.roomId = roomId1;
                    roomDetails.users.unshift(data.senderId);
                    roomDetails.users.unshift(data.recieverId);
                    roomDetails.updated_at = new Date();
                    roomDetails.chats.unshift({ 
                          message: data.message, 
                          by: data.senderId,
                          senderId : data.senderId,
                          senderName : data.senderName,
                          senderProfile : data.senderProfile,
                          recieverId : data.recieverId,
                          receiverName : data.receiverName,
                          receiverProfile : data.receiverProfile,
                          time : dateTime 
                        });  
                        roomDetails.save().then(() => {
                        var notification = new Notification();
                        notification.createdAt = new Date();
                        notification.adminID = recieverId;
                        notification.status = 'unread';
                        notification.details = '';
                        notification.title = 'Vous avez un nouveau message de' + data.name ;
                        notification.sentTo = recieverId;
                        notification.sentBy =  senderId;
                        notification.type = 'Message';
                  
                        notification.save(function(err) {
                          if (err) {
                              res.json({
                                  success: false,
                                  message: err
                              });
                          }
                          User.findById(recieverId, function getUser(err, user1) {
                              if (err) {
                                  console.log(err);
                                   console.log("in error of user",err);
                                  errorMessage = err;
                              } else {
                                  if (user1.device_token != undefined || user1.device_token != null) {
                                      sendPushNotifications(user1, notification.title, notification.type, user1.device_token);
                                    io.sockets.emit(room, roomDetails);
                                  }
                              }
                          });
                      })
                      });

                  }

                   
                
                })
            
        });


        socket.on('typing', data => {
          socket.broadcast.emit('typing', data); // return data
        });
        
});
*/